package com.stat9.ohms.common;

import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RandomNumberGenerator {
	private static Log logger = LogFactory.getLog(RandomNumberGenerator.class);
	
	public static String getRandomNumber(){
		String val =UUID.randomUUID().toString();
		logger.info("Random Number>>>>>>>>>>>"+val);
		return val;
	}
	
}
