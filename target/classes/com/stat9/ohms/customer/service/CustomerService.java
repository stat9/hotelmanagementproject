package com.stat9.ohms.customer.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.stat9.ohms.customer.dao.CustomerDAO;
import com.stat9.ohms.customer.dao.CustomerDAOException;
import com.stat9.ohms.customer.dao.CustomerDO;
import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.UserVO;

@Component(value="customerService")
public class CustomerService {

	private Log logger = LogFactory.getLog(CustomerService.class);
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private Mapper mapper;
	@Transactional
	public Long registerCustomer(CustomerVO customer,UserVO userVO) throws CustomerServiceException {
		logger.info("Entered registerCustomer--service");
		try {
			
			return getCustomerDAO().registerCustomer(customer,userVO);
		} catch (CustomerDAOException e) {
			logger.error("Error occured while registring customer >>>>>>>> "+e);
			throw new CustomerServiceException("Error occured while registring customer >>>>>>>>>> "+e);
		}

	}
	
	@Transactional
	public CustomerVO getCustomerByCode(String customerCode) throws  CustomerServiceException {
		logger.info("Entered getCustomerByCode");
		logger.info("Entered getCustomerByCode--service");
		try {
			CustomerDO customer= getCustomerDAO().getCustomerByCode(customerCode);
			CustomerVO customerVO= mapper.map(customer, CustomerVO.class);
			return customerVO;
		} catch (CustomerDAOException e) {
			logger.error("Error occured while registring customer >>>>>>>> "+e);
			throw new CustomerServiceException("Error occured while registring customer >>>>>>>>>> "+e);
		}
	}

	

	public CustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	public Mapper getMapper() {
		return mapper;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

}
