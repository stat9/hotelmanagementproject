package com.stat9.ohms.customer.service;

import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.UserVO;

public interface ICustomerService {

	Long registerCustomer(CustomerVO customer,UserVO userVO) throws CustomerServiceException;
}
