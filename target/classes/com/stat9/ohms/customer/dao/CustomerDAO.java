package com.stat9.ohms.customer.dao;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.AbstractDAO;
import com.stat9.ohms.common.AddressDO;
import com.stat9.ohms.common.PersonDO;
import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.dao.HotelDAOException;
import com.stat9.ohms.hotel.dao.ItemDO;
import com.stat9.ohms.login.dao.UserDO;

@Component
public class CustomerDAO  extends AbstractDAO implements ICustomerDAO{

	@Autowired
	private Mapper mapper;
	
	
	private Log logger = LogFactory.getLog(CustomerDAO.class);
	
	@Override
	public Long registerCustomer(CustomerVO customerVO,UserVO userVO) throws CustomerDAOException {
		
		logger.info("Entered registerCustomer");
		CustomerDO  customer=null;
		AddressDO address =null;
		PersonDO person=null;
		UserDO user = null;
		try{
		
		customer= mapper.map(customerVO,CustomerDO.class);
		address = mapper.map(customerVO.getAddress(),AddressDO.class);
		person = mapper.map(customerVO.getPerson(),PersonDO.class);
		user= mapper.map(userVO,UserDO.class);
		address.setCreatedBy(user.getUserName());
		address.setCreatedDate(new Date());
		address.setModifiedBy(user.getUserName());
		address.setModifiedDate(new Date());
		person.setCreatedBy(user.getUserName());
		person.setCreatedDate(new Date());
		person.setModifiedBy(user.getUserName());
		person.setModifiedDate(new Date());
		getSession().saveOrUpdate(address);
		getSession().saveOrUpdate(person);
		customer.setAddress(address);
		customer.setPerson(person);
		customer.setCreatedBy(user.getUserName());
		customer.setCreatedDate(new Date());
		customer.setModifiedBy(user.getUserName());
		customer.setModifiedDate(new Date());
		getSession().saveOrUpdate(customer);
		
		
		user.setCreatedBy(user.getUserName());
		user.setCreatedDate(new Date());
		user.setModifiedBy(user.getUserName());
		user.setModifiedDate(new Date());
		getSession().saveOrUpdate(user);
		
		logger.info("Customer Registered successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while registring customer>>>>"+ex);
			throw new CustomerDAOException("Error occured while registring customer>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit registerCustomer");
		return customer.getCustomerId();
	}
	@Override
	public CustomerDO getCustomerByCode(String customerCode) throws CustomerDAOException {
		logger.info("Entered getCustomerByCode");
		CustomerDO customer=null;
		try{
		Query query =getSession().createQuery("from CustomerDO customer where customer.customerCode= '"+customerCode+"'");
		 customer =(CustomerDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getCustomerByCode>>>>"+ex);
			throw new CustomerDAOException("Error occured while getCustomerByCode>>>>>>>>>>>"+ex);
		}
		return customer;
	}

	public Mapper getMapper() {
		return mapper;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	
}
