package com.stat9.ohms.customer.dao;

import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.UserVO;

public interface ICustomerDAO {
	
	Long registerCustomer(CustomerVO customer,UserVO user) throws CustomerDAOException;
	 CustomerDO getCustomerByCode(String customerCode) throws CustomerDAOException;
}
