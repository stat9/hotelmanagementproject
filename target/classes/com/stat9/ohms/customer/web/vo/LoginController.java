package com.stat9.ohms.customer.web.vo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stat9.ohms.hotel.web.vo.LoginVO;
import com.stat9.ohms.login.service.LoginService;
import com.stat9.ohms.login.service.LoginServiceException;

@Controller
@RequestMapping(value="/login" )
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	
	private Log logger = LogFactory.getLog(LoginController.class);
	
	@RequestMapping(method = RequestMethod.POST,value="/validate" )
	public LoginVO validateUser( @RequestBody UserVO user) throws CustomerUIException {
		logger.info("Entered registerCustomer--controller");
		LoginVO login=null;
		try {
			login = getLoginService().validateUser(user.getUserName(), user.getPassword());
		} catch (LoginServiceException e) {
			logger.error("Error occured while registring customer");
			throw new CustomerUIException();

		}
		return login;
		
	}
	
	
	

	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

}
