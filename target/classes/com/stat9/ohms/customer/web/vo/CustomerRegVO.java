package com.stat9.ohms.customer.web.vo;

public class CustomerRegVO {
	
	private UserVO user;
	private CustomerVO customer;
	public UserVO getUser() {
		return user;
	}
	public void setUser(UserVO user) {
		this.user = user;
	}
	public CustomerVO getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerVO customer) {
		this.customer = customer;
	}

}
