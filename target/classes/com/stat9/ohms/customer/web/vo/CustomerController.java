package com.stat9.ohms.customer.web.vo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.stat9.ohms.customer.service.CustomerService;
import com.stat9.ohms.customer.service.CustomerServiceException;

@Controller
@RequestMapping(value="/customer" )
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	private Log logger = LogFactory.getLog(CustomerController.class);
	
	@RequestMapping(method = RequestMethod.POST,value="/register" )
	public String registerCustomer( @RequestBody CustomerRegVO customer) throws CustomerUIException {
		logger.info("Entered registerCustomer--controller");
		Long customerId=null;
		try {
			customerId = getCustomerService().registerCustomer(customer.getCustomer(),customer.getUser());
		} catch (CustomerServiceException e) {
			logger.error("Error occured while registring customer");
			throw new CustomerUIException();

		}
		return customerId!=null?"customer":"error";
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/customer")
	public CustomerVO getCustomerByCode(@RequestParam String customerCode) throws CustomerUIException {
		logger.info("Entered getItemByCode--controller");
		CustomerVO customer = null;
		try {
			customer = getCustomerService().getCustomerByCode(customerCode);
		} catch (CustomerServiceException e) {
			logger.error("Error occured while getItemByCode");
			throw new CustomerUIException();

		}
		return customer ;

	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}


}
