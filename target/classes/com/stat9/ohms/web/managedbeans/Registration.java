package com.stat9.ohms.web.managedbeans;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.RandomNumberGenerator;
import com.stat9.ohms.customer.web.vo.AddressVO;
import com.stat9.ohms.customer.web.vo.CustomerController;
import com.stat9.ohms.customer.web.vo.CustomerRegVO;
import com.stat9.ohms.customer.web.vo.CustomerUIException;
import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.PersonVO;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.vo.HotelRegVO;
import com.stat9.ohms.hotel.web.vo.HotelVO;

@Component
@Scope("request")
public class Registration {

	private Log logger = LogFactory.getLog(Registration.class);

	@Autowired
	private CustomerController customerController;
	
	@Autowired
	private HotelController hotelController;

	private String hotelName;

	private String userName;

	private String password;

	private String firstName;

	private String lastName;

	private String middleName;

	private String gender;

	private String mobileNumber;

	private String email;

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private String city;

	private String state;

	private String zipCode;
	
	private  String customerCode;
	
	private  String hotelCode;

	private boolean hotel = false;
	
	 @PostConstruct
	public void init(){
		setHotelCode("HT"+RandomNumberGenerator.getRandomNumber());
		setCustomerCode("CT"+RandomNumberGenerator.getRandomNumber());
	}

	public Boolean isHotelRegistration() {
		return hotel;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public boolean isHotel() {
		return hotel;
	}

	public void setHotel(boolean hotel) {
		this.hotel = hotel;
	}

	public String registration() throws CustomerUIException {
		String retString=null;
		logger.info("Entered validateUser");
		try {
			PersonVO person = new PersonVO();
			AddressVO address = new AddressVO();
			person.setFirstName(firstName);
			person.setLastName(lastName);
			person.setMiddleName(middleName);
			person.setMobileNumber(mobileNumber);
			person.setGender(gender);
			person.setEmail(email);
			person.setMobileNumber(mobileNumber);
			address.setAddressLine1(addressLine1);
			address.setAddressLine2(addressLine2);
			address.setAddressLine3(addressLine3);
			address.setCity(city);
			address.setState(state);
			address.setZipCode(zipCode);
			CustomerVO customer = new CustomerVO();
			customer.setAddress(address);
			customer.setPerson(person);
			customer.setCustomerCode(getCustomerCode());
			HotelVO hotelVO = new HotelVO();
			hotelVO.setHotelName(hotelName);
			hotelVO.setHotelCode(getHotelCode());
			logger.info("logger info>>>>>>>>>>>>>hotelVO.getHotelCode()>>>>>>>>"+hotelVO.getHotelCode());
			hotelVO.setAddress(address);
			hotelVO.setPerson(person);
			UserVO user = new UserVO();
			user.setPassword(password);
			user.setUserName(userName);
			CustomerRegVO regVO = new CustomerRegVO();
			HotelRegVO hotelRegVO = new HotelRegVO();
			hotelRegVO.setHotel(hotelVO);
			hotelRegVO.setUser(user);
			regVO.setCustomer(customer);
			regVO.setUser(user);
			if (hotelName == null) {
				user.setUserCode(customer.getCustomerCode());
				retString = getCustomerController().registerCustomer(regVO);
				if(retString!=null){
					// Set the message here
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your registration is done successfully", "customer"); 
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("customer", customer);
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("isHotelLoggedIn", false);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}
				return retString;
			}else{
				user.setUserCode(hotelVO.getHotelCode());
				retString = getHotelController().registerHotel(hotelRegVO);
				if(retString!=null){
					// Set the message here
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Hotel Registred successfully", "addMenu");  
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotelVO);
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("isHotelLoggedIn", true);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}
				return retString;
			}
		} catch (Exception ex) {
			logger.error("error while validate User");
			throw new CustomerUIException();
		}

	}

	public CustomerController getCustomerController() {
		return customerController;
	}

	public void setCustomerController(CustomerController customerController) {
		this.customerController = customerController;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public HotelController getHotelController() {
		return hotelController;
	}

	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}

	
}
