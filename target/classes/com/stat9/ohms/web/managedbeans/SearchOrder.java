package com.stat9.ohms.web.managedbeans;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.customer.web.vo.CustomerController;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.OrderStatusVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;

@Component
@Scope("request")
public class SearchOrder {
	@Autowired
	private HotelController hotelController;

	@Autowired
	private CustomerController customerController;
	private Log logger = LogFactory.getLog(SearchOrder.class);

	private List<OrderVO> orderList;

	private OrderVO selectedOrder;
	private OrderVO viewedOrder= new OrderVO();
	private Double orderTotal=0.0;
	private Long orderId=new Long(0);
	private Date dateFrom;
	private Map<String,String> status;
	private String orderStatus="";
	private Date dateTo;

	@PostConstruct
	public void init() {
		orderList = (List<OrderVO>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("orderList");
		status  = new HashMap<String, String>();
		status.put("DRAFT", "DRAFT");
		status.put("INPROCESS", "INPROCESS");
		status.put("DELIVERED", "DELIVERED");
		if( null!=FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrder")){
			selectedOrder =(OrderVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrder");
		}
		if( null!=FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrderStatus")){
			orderStatus =(String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrderStatus");
		}
		
	}
	
	public void onStatusChange(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedOrderStatus", getOrderStatus());
	}
	public void updateSelectedOrder(SelectEvent event) {
		selectedOrder = ((OrderVO) event.getObject());
	    }
	public void viewOrder(){
		viewedOrder =selectedOrder;
	}
	

	public String searchOrder() throws HotelUIException {
		logger.info("Entered searchOrder");
		try {
			orderList = getHotelController().searchOrder(dateFrom,dateTo);
			if (orderList != null && orderList.size() > 0) {
				OrderVO order = orderList.get(0);
				HotelVO hotel = order.getHotel();
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotel);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderList", orderList);
			}

			logger.info("orders retrieved in search>>>>>>>>>>>>>>>>>" + orderList.size());
		} catch (Exception ex) {
			logger.error("Error occured at searchOrder" + ex);
			throw new HotelUIException("Error occured at searchOrder " + ex);
		}

		return orderList != null && orderList.size() > 0 ? "searchorder" : "searchorder";

	}

	public String editOrder() throws HotelUIException{
		logger.info("Entered editOrder");
		String retString="";
		try {
			if( null!=FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrder")){
				selectedOrder =(OrderVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrder");
			}
			if( null!=FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrderStatus")){
				orderStatus =(String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedOrderStatus");
			}
			OrderStatusVO orderStatusVO = getHotelController().getOrderStatusByCode(orderStatus);
			selectedOrder.setOrderStatus(orderStatusVO);
			UserVO user =(UserVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			retString = getHotelController().editOrder(selectedOrder, user);
			if(null!=retString){
				
			logger.info("Order selected in search>>>>>>>>>>>>>>>>>" + selectedOrder);
				HotelVO hotel = selectedOrder.getHotel();
				FacesMessage msg = new FacesMessage("Your Order edited successfully");
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotel);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", selectedOrder);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

		} catch (Exception ex) {
			logger.error("Error occured at editOrder" + ex);
			throw new HotelUIException("Error occured at editOrder " + ex);
		}

		return retString;
	}
	
	public void updateOrder(SelectEvent event) {
		selectedOrder = ((OrderVO) event.getObject());
	    }
		
		public void updateEditOrder() {
			setOrderId(selectedOrder.getOrderId());
			setOrderStatus(selectedOrder.getOrderStatus().getOrderStatusCode());
			
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedOrder",selectedOrder);
			}
		

	public HotelController getHotelController() {
		return hotelController;
	}

	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}

	public CustomerController getCustomerController() {
		return customerController;
	}

	public void setCustomerController(CustomerController customerController) {
		this.customerController = customerController;
	}

	public List<OrderVO> getItemList() {
		return orderList;
	}

	public void setItemList(List<OrderVO> itemList) {
		this.orderList = itemList;
	}


	public Double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public OrderVO getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(OrderVO selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public List<OrderVO> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrderVO> orderList) {
		this.orderList = orderList;
	}

	public Map<String, String> getStatus() {
		return status;
	}

	public void setStatus(Map<String, String> status) {
		this.status = status;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public OrderVO getViewedOrder() {
		return viewedOrder;
	}

	public void setViewedOrder(OrderVO viewedOrder) {
		this.viewedOrder = viewedOrder;
	}

		

}
