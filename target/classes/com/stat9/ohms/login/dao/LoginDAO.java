package com.stat9.ohms.login.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.AbstractDAO;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.vo.LoginVO;

@Component
public class LoginDAO extends AbstractDAO  implements ILoginDAO{
	@Autowired
	private Mapper mapper;
	
	private Log logger = LogFactory.getLog(LoginDAO.class);
	
	@Override
	public LoginVO validateUser(String userName,String password) throws LoginDAOException {
		logger.info("Entered validateUser>>>>"+userName);
		LoginVO login = null;
		String retString =null;
		try{
			Criteria criteria = getSession().createCriteria(UserDO.class);
			criteria.add(Restrictions.eq("userName", userName)).add(Restrictions.eq("password", password));
			UserDO user= (UserDO)criteria.uniqueResult();
			if(null!=user){
				UserVO userVO =mapper.map(user, UserVO.class);
				login =new LoginVO();
				login.setUserVO(userVO);
				logger.info("User>>>>"+user.getUserName());
				Query query= getSession().createQuery("from CustomerDO customer where customer.customerCode ='"+user.getUserCode()+"'");
				if(query.list()!=null && !query.list().isEmpty()){
					retString = "customer";
				}
				
				Query hotelquery= getSession().createQuery("from HotelDO hotel where hotel.hotelCode ='"+user.getUserCode()+"'");
				if(hotelquery.list()!=null && !hotelquery.list().isEmpty()){
					retString = "hotel";
				}
				login.setReturnString(retString);
			}
		
		}catch(HibernateException ex){
			logger.error("Error occured while validating user>>>>"+ex);
			throw  new LoginDAOException("Error occured while validating user>>>>"+ex);
		}
		
		logger.info("exit validateUser");
		return login;
	}
	public Mapper getMapper() {
		return mapper;
	}
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

}
