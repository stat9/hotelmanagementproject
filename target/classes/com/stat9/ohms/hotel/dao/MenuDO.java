package com.stat9.ohms.hotel.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name = "MENU_T")
@Entity
public class MenuDO {

	

	@Id
	@GeneratedValue
	@Column(name = "MENU_ID")
	private Long menuId;
	
	@Column(name = "MENU_NAME")
	private String menuName;
	
	@Column(name = "MENU_CD")
	private String menuCode;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="HOTEL_ID",referencedColumnName="HOTEL_ID")
	private HotelDO hotel;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "menu")
	private Set<ItemDO> itemList = new HashSet<ItemDO>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
	private Set<OrderItemDO> orderItemList = new HashSet<OrderItemDO>();
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public HotelDO getHotel() {
		return hotel;
	}

	public void setHotel(HotelDO hotel) {
		this.hotel = hotel;
	}

	public Set<ItemDO> getItemList() {
		return itemList;
	}

	public void setItemList(Set<ItemDO> itemList) {
		this.itemList = itemList;
	}

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public Set<OrderItemDO> getOrderItemList() {
		return orderItemList;
	}

	public void setOrderItemList(Set<OrderItemDO> orderItemList) {
		this.orderItemList = orderItemList;
	}
}
