package com.stat9.ohms.hotel.dao;

public class HotelDAOException extends Exception {

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public HotelDAOException() {
		super();
	}
	
	public HotelDAOException(String message) {
		super(message);
	}
}
