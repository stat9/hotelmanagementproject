package com.stat9.ohms.hotel.dao;

import java.util.Date;
import java.util.List;

import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;

public interface IHotelDAO {

	Long registerHotel(HotelVO hotel, UserVO user) throws HotelDAOException;

	Long addMenu(MenuVO menu, UserVO user) throws HotelDAOException;

	Long addItem(MenuVO menu, UserVO user,ItemVO item) throws HotelDAOException;

	Long placeOrder(OrderVO orderVO, UserVO userVO) throws HotelDAOException;

	HotelDO getHotelByCode(String hotelCode) throws HotelDAOException;

	OrderDO getOrderByCode(String orderCode) throws HotelDAOException;

	OrderStatusDO getOrderStatusByCode(String orderStatusCode) throws HotelDAOException;

	MenuDO getMenuByCode(String menuCode) throws HotelDAOException;

	ItemDO getItemByCode(String itemCode) throws HotelDAOException;

	List<ItemDO> searchMenu(String hotelName, String menuName, Date dateFrom, Date dateTo) throws HotelDAOException;

	Long editItem(ItemVO itemVO, UserVO userVO) throws HotelDAOException;

	List<OrderDO> searchOrder(Date dateFrom, Date dateTo) throws HotelDAOException;

	Long editOrder(OrderVO orderVO, UserVO userVO) throws HotelDAOException;

	OrderDO getOrderByOrderId(Long orderId) throws HotelDAOException;
	
	public List<OrderItemDO> getOrderItemsByOrderId(Long orderId) throws HotelDAOException;
	
	public Long deleteItem(ItemVO itemVO,UserVO userVO) throws HotelDAOException;
}
