package com.stat9.ohms.hotel.web.vo;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class ItemVODataModel extends ListDataModel<ItemVO> implements SelectableDataModel<ItemVO> {

    public ItemVODataModel(List<ItemVO> list) {
        super(list);
    }

    @Override
    public Object getRowKey(ItemVO t) {
        return t.getItemCode();
    }

    @Override
    public ItemVO getRowData(String string) {
        List<ItemVO> itemList=(List<ItemVO>) getWrappedData();
        for(ItemVO item:itemList){
            if(item.getItemCode().equalsIgnoreCase(string)){
                return item;
            }
        }
        return null;
    }
    
    

}
