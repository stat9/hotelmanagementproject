package com.stat9.ohms.hotel.web.vo;

import com.stat9.ohms.customer.web.vo.UserVO;

public class LoginVO {
	
	private String returnString;
	
	private UserVO userVO;

	public String getReturnString() {
		return returnString;
	}

	public void setReturnString(String returnString) {
		this.returnString = returnString;
	}

	public UserVO getUserVO() {
		return userVO;
	}

	public void setUserVO(UserVO userVO) {
		this.userVO = userVO;
	}

}
