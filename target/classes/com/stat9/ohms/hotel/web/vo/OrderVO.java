package com.stat9.ohms.hotel.web.vo;

import java.util.ArrayList;
import java.util.List;

import com.stat9.ohms.customer.web.vo.CustomerVO;

public class OrderVO {

	private CustomerVO customer;
	private Long orderId;
	private HotelVO hotel;
	private String orderCode;
	private Double orderTotal;
	
	private OrderStatusVO orderStatus;
	private List<ItemVO> itemList = new ArrayList<ItemVO>();

	public CustomerVO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerVO customer) {
		this.customer = customer;
	}

	public HotelVO getHotel() {
		return hotel;
	}

	public void setHotel(HotelVO hotel) {
		this.hotel = hotel;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public List<ItemVO> getItemList() {
		return itemList;
	}

	public void setItemList(List<ItemVO> itemList) {
		this.itemList = itemList;
	}

	public Double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public OrderStatusVO getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatusVO orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	


	
}
