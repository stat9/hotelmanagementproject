package com.stat9.ohms.hotel.web.vo;

import com.stat9.ohms.customer.web.vo.UserVO;

public class OrderAddVO {

private OrderVO order;
private UserVO user;
public OrderVO getOrder() {
	return order;
}
public void setOrder(OrderVO order) {
	this.order = order;
}
public UserVO getUser() {
	return user;
}
public void setUser(UserVO user) {
	this.user = user;
}
	
}
