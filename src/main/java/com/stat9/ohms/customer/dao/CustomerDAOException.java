package com.stat9.ohms.customer.dao;

public class CustomerDAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public CustomerDAOException() {
		super();
	}
	
	public CustomerDAOException(String message) {
		super(message);
	}


}
