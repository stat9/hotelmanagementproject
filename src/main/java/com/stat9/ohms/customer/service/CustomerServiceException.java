package com.stat9.ohms.customer.service;

public class CustomerServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public CustomerServiceException() {
		super();
	}
	
	public CustomerServiceException(String message) {
		super(message);
	}

}
