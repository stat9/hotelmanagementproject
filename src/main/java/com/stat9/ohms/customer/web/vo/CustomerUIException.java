package com.stat9.ohms.customer.web.vo;

public class CustomerUIException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public CustomerUIException() {
		super();
	}
	
	public CustomerUIException(String message) {
		super(message);
	}
}
