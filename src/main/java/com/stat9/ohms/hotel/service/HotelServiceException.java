package com.stat9.ohms.hotel.service;

public class HotelServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public HotelServiceException() {
		super();
	}
	
	public HotelServiceException(String message) {
		super(message);
	}
}
