package com.stat9.ohms.hotel.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.dao.HotelDAOException;
import com.stat9.ohms.hotel.dao.HotelDO;
import com.stat9.ohms.hotel.dao.MenuDO;
import com.stat9.ohms.hotel.dao.RuleDAO;
import com.stat9.ohms.hotel.dao.RuleDO;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.RuleVO;

@Component
public class RuleService {
	private Log logger = LogFactory.getLog(RuleService.class);
	@Autowired
	private RuleDAO ruleDAO;
	
	@Autowired
	private Mapper mapper;
	
	@Transactional
	public RuleVO getRuleByName(String ruleName) throws HotelServiceException {
		logger.info("Entered getRuleByName");
		RuleVO ruleVO =null;
		try{
			RuleDO rule=getRuleDAO().getRuleByName(ruleName);
			if(rule!=null)
			ruleVO= mapper.map(rule, RuleVO.class);
		}catch(HotelDAOException ex){
			logger.error("Error occured while getRuleByName >>>>>>>> "+ex);
			throw new HotelServiceException("Error occured while getRuleByName >>>>>>>> "+ex);
		}
		return ruleVO;
		
	}
	
	@Transactional
	public List<MenuVO> getMenuByHotelName(String hotelName) throws HotelServiceException {
		logger.info("Entered getRuleByName");
		List<MenuVO> menuList =new ArrayList<MenuVO>();
		try{
			List<MenuDO> rule=getRuleDAO().getMenuByHotelName(hotelName);
			for (MenuDO menuDO : rule) {
				MenuVO menu = mapper.map(menuDO, MenuVO.class);
				menuList.add(menu);
			}
		}catch(HotelDAOException ex){
			logger.error("Error occured while getRuleByName >>>>>>>> "+ex);
			throw new HotelServiceException("Error occured while getRuleByName >>>>>>>> "+ex);
		}
		return menuList;
		
	}
	
	@Transactional
	public Long addRule(RuleVO ruleVO, HotelVO hotelVO,UserVO user) throws HotelServiceException {
		logger.info("Entered addRule");
		RuleDO rule=null;
		HotelDO hotel=null;
		try{
			rule= mapper.map(ruleVO, RuleDO.class);
			rule.setCreatedDate(new Date());
			rule.setCreatedBy(user.getUserName());
			rule.setModifiedBy(user.getUserName());
			hotel = mapper.map(hotelVO, HotelDO.class);
			getRuleDAO().addRule(rule, hotel);
		}catch(HotelDAOException ex){
			logger.error("Error occured while addRule >>>>>>>> "+ex);
			throw new HotelServiceException("Error occured while addRule >>>>>>>> "+ex);
		}
		return rule.getRuleId();
	}
	
	@Transactional
	public void deleteRule(RuleVO ruleVO) throws HotelServiceException {
		logger.info("Entered deleteRule");
		RuleDO rule=null;
		try{
			rule= mapper.map(ruleVO, RuleDO.class);
			getRuleDAO().deleteRule(rule);
		}catch(HotelDAOException ex){
			logger.error("Error occured while deleteRule >>>>>>>> "+ex);
			throw new HotelServiceException("Error occured while deleteRule >>>>>>>> "+ex);
		}
	}

	public RuleDAO getRuleDAO() {
		return ruleDAO;
	}

	public void setRuleDAO(RuleDAO ruleDAO) {
		this.ruleDAO = ruleDAO;
	}

}
