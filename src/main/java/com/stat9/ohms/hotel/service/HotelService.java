package com.stat9.ohms.hotel.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.dao.HotelDAO;
import com.stat9.ohms.hotel.dao.HotelDAOException;
import com.stat9.ohms.hotel.dao.HotelDO;
import com.stat9.ohms.hotel.dao.ItemDO;
import com.stat9.ohms.hotel.dao.OrderDO;
import com.stat9.ohms.hotel.dao.OrderItemDO;
import com.stat9.ohms.hotel.dao.OrderStatusDO;
import com.stat9.ohms.hotel.dao.RuleDO;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.OrderItemVO;
import com.stat9.ohms.hotel.web.vo.OrderStatusVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;
@Component
public class HotelService {

	private Log logger = LogFactory.getLog(HotelService.class);
	@Autowired
	private HotelDAO hotelDAO;
	
	@Autowired
	private Mapper mapper;
	@Transactional
	public Long registerHotel(HotelVO hotel,UserVO user) throws HotelServiceException{
		logger.info("Entered registerHotel--service");
		try {
			return getHotelDAO().registerHotel(hotel,user);
		} catch (HotelDAOException e) {
			logger.error("Error occured while registring hotel >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while registring hotel >>>>>>>>>> "+e);
		}
		
	}
	@Transactional
	public Long addMenu(MenuVO menu, UserVO user) throws HotelServiceException {
		logger.info("Entered addMenu--service");
		try {
			return getHotelDAO().addMenu(menu,user);
		} catch (HotelDAOException e) {
			logger.error("Error occured while adding Menu >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while adding Menu >>>>>>>>>> "+e);
		}
	}
	
	@Transactional
	public List<ItemVO> searchMenu(String hotelName, String menuName,Date dateFrom,Date dateTo) throws HotelServiceException {
		logger.info("Entered searchMenu--service");
		List<ItemVO>  itemList = new ArrayList<ItemVO>();
		try {
			List<ItemDO> items= getHotelDAO().searchMenu(hotelName,menuName,dateFrom,dateTo);
			logger.info("Items retrieved in search>>>>>>>>>>>>>>>>>"+items.size());
			for (ItemDO itemDO : items) {
				ItemVO item=mapper.map(itemDO, ItemVO.class);
				logger.info("Items added>>>>>>>>>>>>>>>>>"+item.getItemCode());
				itemList.add(item);
			}
		} catch (HotelDAOException e) {
			logger.error("Error occured while searching Menu >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while searching Menu >>>>>>>>>> "+e);
		}
		
		return itemList;
	}
	
	@Transactional
	public void deleteItemFromOrder(OrderItemVO itemVO,OrderVO orderVO,UserVO userVO) throws HotelServiceException{
		logger.info("Entered deleteItemFromOrder--service");
		try {
			 getHotelDAO().deleteItemFromOrder(itemVO, orderVO, userVO);
		} catch (HotelDAOException e) {
			logger.error("Error occured while deleteItemFromOrder >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while deleteItemFromOrder >>>>>>>>>> "+e);
		}
		
	}
	
	
	@Transactional
	public List<ItemVO> searchMenuForCustomer(String hotelName) throws HotelServiceException {
		logger.info("Entered searchMenuForCustomer--service");
		List<ItemVO>  itemList = new ArrayList<ItemVO>();
		try {
			List<ItemDO> items= getHotelDAO().searchMenuForCustomer(hotelName);
			logger.info("Items retrieved in searchMenuForCustomer>>>>>>>>>>>>>>>>>"+items.size());
			for (ItemDO itemDO : items) {
				ItemVO item=mapper.map(itemDO, ItemVO.class);
				logger.info("Items added>>>>>>>>>>>>>>>>>"+item.getItemCode());
				itemList.add(item);
			}
		} catch (HotelDAOException e) {
			logger.error("Error occured while searchMenuForCustomer >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while searchMenuForCustomer >>>>>>>>>> "+e);
		}
		
		return itemList;
	}
	
	@Transactional
	public List<OrderVO> searchOrder(Date dateFrom,Date dateTo) throws HotelServiceException {
		logger.info("Entered searchOrder--service");
		List<OrderVO>  orderList = new ArrayList<OrderVO>();
		try {
			List<OrderDO> orders= getHotelDAO().searchOrder(dateFrom,dateTo);
			logger.info("Orders retrieved in search>>>>>>>>>>>>>>>>>"+orders.size());
			for (OrderDO orderDO : orders) {
				OrderVO order=mapper.map(orderDO, OrderVO.class);
				logger.info("Orders added>>>>>>>>>>>>>>>>>"+order.getOrderCode());
				orderList.add(order);
			}
		} catch (HotelDAOException e) {
			logger.error("Error occured while searching Order >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while searching order >>>>>>>>>> "+e);
		}
		
		return orderList;
	}
	
	@Transactional
	public Long addItem(MenuVO menu, UserVO user,ItemVO item) throws HotelServiceException {
		logger.info("Entered addItem--service");
		try {
			return getHotelDAO().addItem(menu,user,item);
		} catch (HotelDAOException e) {
			logger.error("Error occured while adding Item >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while adding Item >>>>>>>>>> "+e);
		}
	}
	
	@Transactional
	public Long editItem(ItemVO item, UserVO user) throws HotelServiceException {
		logger.info("Entered editItem--service");
		try {
			return getHotelDAO().editItem(item,user);
		} catch (HotelDAOException e) {
			logger.error("Error occured while edit Item >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while edit Item >>>>>>>>>> "+e);
		}
	}
	
	@Transactional
	public Long deleteItem(ItemVO item, UserVO user) throws HotelServiceException {
		logger.info("Entered editItem--service");
		try {
			return getHotelDAO().deleteItem(item,user);
		} catch (HotelDAOException e) {
			logger.error("Error occured while edit Item >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while edit Item >>>>>>>>>> "+e);
		}
	}
	
	@Transactional
	public Long editOrder(OrderVO order, UserVO user) throws HotelServiceException {
		logger.info("Entered editOrder--service");
		try {
			return getHotelDAO().editOrder(order,user);
		} catch (HotelDAOException e) {
			logger.error("Error occured while editOrder >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while edit Order >>>>>>>>>> "+e);
		}
	}
	@Transactional
	public Long placeOrder(OrderVO order, UserVO user) throws HotelServiceException {
		logger.info("Entered addOrder--service");
		try {
			return getHotelDAO().placeOrder(order,user);
		} catch (HotelDAOException e) {
			logger.error("Error occured while adding Order >>>>>>>> "+e);
			throw new HotelServiceException("Error occured while adding Order >>>>>>>>>> "+e);
		}
	}
	
	@Transactional
	public ItemVO getItemByCode(String itemCode) throws HotelServiceException {
		logger.info("Entered getItemByCode--service");
		ItemVO itemVO = null;
		
		try {
			ItemDO item = getHotelDAO().getItemByCode(itemCode);
			itemVO=mapper.map(item, ItemVO.class);
		} catch (HotelDAOException e) {
			logger.error("Error occured while getItemByCode");
			throw new HotelServiceException();

		}
		return itemVO ;

	}
	
	@Transactional
	public HotelVO getHotelByCode(String hotelCode) throws HotelServiceException {
		logger.info("Entered getHotelByCode--service");
		HotelVO hotelVO = null;
		
		try {
			HotelDO hotel = getHotelDAO().getHotelByCode(hotelCode);
			hotelVO=mapper.map(hotel, HotelVO.class);
		} catch (HotelDAOException e) {
			logger.error("Error occured while getHotelByCode");
			throw new HotelServiceException();

		}
		return hotelVO ;

	}
	
	@Transactional
	public OrderVO getOrderByOrderId(Long orderId) throws HotelServiceException {
		logger.info("Entered getOrderByOrderId");
		OrderVO orderVO =null;
		
		try{
			OrderDO order =getHotelDAO().getOrderByOrderId(orderId);
			orderVO =mapper.map(order, OrderVO.class);
		}catch(HotelDAOException ex){
			logger.error("Error occured while getOrderByOrderId>>>>"+ex);
			throw new HotelServiceException("Error occured while getOrderByOrderId>>>>>>>>>>>"+ex);
		}
		return orderVO;
	}
	
	@Transactional
	public OrderStatusVO getOrderStatusByCode(String orderStatusCode)  throws HotelServiceException {
		logger.info("Entered getOrderStatusByCode");
		OrderStatusVO orderStatusVO =null;
		
		try{
			OrderStatusDO status =getHotelDAO().getOrderStatusByCode(orderStatusCode);
			orderStatusVO =mapper.map(status, OrderStatusVO.class);
		}catch(HotelDAOException ex){
			logger.error("Error occured while getOrderStatusByCode>>>>"+ex);
			throw new HotelServiceException("Error occured while getOrderStatusByCode>>>>>>>>>>>"+ex);
		}
		return orderStatusVO;
	}
	
	@Transactional
	public List<OrderItemVO> getOrderItemsByOrderId(Long orderId) throws HotelServiceException {
		logger.info("Entered getOrderItemsByOrderId");
		List<OrderItemVO> items =new ArrayList<OrderItemVO>();
		try{
		
			List<OrderItemDO>  orderItems =getHotelDAO().getOrderItemsByOrderId(orderId);
			for (OrderItemDO orderItemDO : orderItems) {
				OrderItemVO item =mapper.map(orderItemDO, OrderItemVO.class);
				items.add(item);
			}
		}catch(HotelDAOException ex){
			logger.error("Error occured while getOrderItemsByOrderId>>>>"+ex);
			throw new HotelServiceException("Error occured while getOrderItemsByOrderId>>>>>>>>>>>"+ex);
		}
		return items;
	}
	
	
	
	public HotelDAO getHotelDAO() {
		return hotelDAO;
	}

	public void setHotelDAO(HotelDAO hotelDAO) {
		this.hotelDAO = hotelDAO;
	}
	public Mapper getMapper() {
		return mapper;
	}
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
}
