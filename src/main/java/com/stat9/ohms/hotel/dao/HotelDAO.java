package com.stat9.ohms.hotel.dao;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.AbstractDAO;
import com.stat9.ohms.common.AddressDO;
import com.stat9.ohms.common.PersonDO;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.OrderItemVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;
import com.stat9.ohms.login.dao.UserDO;
@Component
public class HotelDAO extends AbstractDAO implements IHotelDAO{

	@Autowired
	private Mapper mapper;
	
	@Autowired
	private IRuleDAO ruleDAO;
	
	private Log logger = LogFactory.getLog(HotelDAO.class);
	@Override
	public Long registerHotel(HotelVO hotelVO, UserVO userVO) throws HotelDAOException {
		logger.info("Entered registerhotel");
		HotelDO  hotel=null;
		AddressDO address =null;
		PersonDO person=null;
		UserDO user = null;
		try{
		
		hotel= mapper.map(hotelVO,HotelDO.class);
		address = mapper.map(hotelVO.getAddress(),AddressDO.class);
		person = mapper.map(hotelVO.getPerson(),PersonDO.class);
		user= mapper.map(userVO,UserDO.class);
		address.setCreatedBy(user.getUserName());
		address.setCreatedDate(new Date());
		address.setModifiedBy(user.getUserName());
		address.setModifiedDate(new Date());
		person.setCreatedBy(user.getUserName());
		person.setCreatedDate(new Date());
		person.setModifiedBy(user.getUserName());
		person.setModifiedDate(new Date());
		getSession().saveOrUpdate(address);
		getSession().saveOrUpdate(person);
		hotel.setAddress(address);
		hotel.setPerson(person);
		hotel.setCreatedBy(user.getUserName());
		hotel.setCreatedDate(new Date());
		hotel.setModifiedBy(user.getUserName());
		hotel.setModifiedDate(new Date());
		logger.info("logger info>>>>>>>>>>>>>hotelVO.getHotelCode()>>>>>>>>"+hotel.getHotelCode());
		getSession().saveOrUpdate(hotel);
		
		user.setCreatedBy(user.getUserName());
		user.setCreatedDate(new Date());
		user.setModifiedBy(user.getUserName());
		user.setModifiedDate(new Date());
		getSession().saveOrUpdate(user);
		
		logger.info("Hotel Registered successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while registring hotel>>>>"+ex);
			throw new HotelDAOException("Error occured while registring hotel>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit registerCustomer");
		return hotel.getHotelId();
	}
	public Mapper getMapper() {
		return mapper;
	}
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	@Override
	public Long addMenu(MenuVO menuVO, UserVO userVO) throws HotelDAOException {
		logger.info("Entered addMenu");
		MenuDO  menu=null;
		HotelDO  hotel=null;
		try{
		
			menu= mapper.map(menuVO,MenuDO.class);
		HotelVO hotelVO = menuVO.getHotel();
		hotel =getHotelByCode(hotelVO.getHotelCode());
		menu.setHotel(hotel);
		menu.setCreatedBy(userVO.getUserName());
		menu.setCreatedDate(new Date());
		menu.setModifiedBy(userVO.getUserName());
		menu.setModifiedDate(new Date());
		
		getSession().saveOrUpdate(menu);
		for (ItemVO itemVO : menuVO.getItemList()) {
			ItemDO item = mapper.map(itemVO,ItemDO.class);
			logger.info("Add Item itemName>>>>>>>>>"+item.getItemName());
			item.setCreatedBy(userVO.getUserName());
			item.setCreatedDate(new Date());
			item.setModifiedBy(userVO.getUserName());
			item.setModifiedDate(new Date());
			item.setMenu(menu);
			getSession().saveOrUpdate(item);
		}
		
		
		logger.info("Add Menu successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while Add Menu>>>>"+ex);
			throw new HotelDAOException("Error occured while Add Menu>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit Add Menu");
		return menu.getMenuId();
	}
	
	@Override
	public Long addItem(MenuVO menuVO, UserVO userVO,ItemVO itemVO) throws HotelDAOException {
		logger.info("Entered addItem");
		MenuDO  menu=null;
		try{
			
			menu = getMenuByCode(menuVO.getMenuCode());
			
	
				ItemDO item = mapper.map(itemVO,ItemDO.class);
				logger.info("Add Item itemName>>>>>>>>>"+item.getItemName());
				item.setCreatedBy(userVO.getUserName());
				item.setCreatedDate(new Date());
				item.setModifiedBy(userVO.getUserName());
				item.setModifiedDate(new Date());
				item.setMenu(menu);
				getSession().saveOrUpdate(item);
				
		
		
		
		logger.info("Add Item successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while Add Item>>>>"+ex);
			throw new HotelDAOException("Error occured while Add Item>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit Add Item");
		return menu.getMenuId();
	}
	
	@Override
	public Long editItem(ItemVO itemVO,UserVO userVO) throws HotelDAOException {
		logger.info("Entered editItem---DAO");
		MenuDO  menu=null;
		ItemDO item =null;
		try{
		
			menu = getMenuByCode(itemVO.getMenu().getMenuCode());
			
			
				item = mapper.map(itemVO,ItemDO.class);
				if(item.getItemId()==null){
					item =getItemByCode(itemVO.getItemCode());
				}
				logger.info("Edit Item itemName>>>>>>>>>"+item.getItemName());
				item.setCreatedBy(userVO.getUserName());
				item.setCreatedDate(new Date());
				item.setModifiedBy(userVO.getUserName());
				item.setModifiedDate(new Date());
				item.setMenu(menu);
				getSession().saveOrUpdate(item);
			
			
		
		
		
		logger.info("Edit Item successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while Edit Item>>>>"+ex);
			throw new HotelDAOException("Error occured while Edit Item>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit Edit Item");
		return item.getItemId();
	}
	
	@Override
	public Long deleteItem(ItemVO itemVO,UserVO userVO) throws HotelDAOException {
		logger.info("Entered deleteItem---DAO");
		MenuDO  menu=null;
		ItemDO item =null;
		try{
		
			menu = getMenuByCode(itemVO.getMenu().getMenuCode());
			
			
				item = mapper.map(itemVO,ItemDO.class);
				
				Set<ItemDO> items= menu.getItemList();
				items.remove(item);
				menu.setItemList(items);
				menu.setModifiedBy(userVO.getUserName());
				menu.setModifiedDate(new Date());
				getSession().saveOrUpdate(menu);
			
			
		
		
		
		logger.info("deleteItem successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while Edit Item>>>>"+ex);
			throw new HotelDAOException("Error occured while Edit Item>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit Edit Item");
		return item.getItemId();
	}
	
	@Override
	public void deleteItemFromOrder(OrderItemVO itemVO,OrderVO orderVO,UserVO userVO) throws HotelDAOException {
		logger.info("Entered deleteItem---DAO");
		OrderDO  order=null;
		ItemDO item =null;
		try{
		
			order = getOrderByCode(orderVO.getOrderCode());
			
			
				item = mapper.map(itemVO,ItemDO.class);
				
				Set<OrderItemDO> items= order.getItemList();
				items.remove(item);
				order.setItemList(items);
				order.setModifiedBy(userVO.getUserName());
				order.setModifiedDate(new Date());
				getSession().saveOrUpdate(order);
			
			
		
		
		
		logger.info("deleteItem successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while deleteItem>>>>"+ex);
			throw new HotelDAOException("Error occured while deleteItem>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit Edit Item");
		
	}
	
	@Override
	public Long placeOrder(OrderVO orderVO, UserVO userVO) throws HotelDAOException {
		logger.info("Entered placeOrder");
		OrderDO  order=null;
		try{
		
			
			order = mapper.map(orderVO,OrderDO.class);
			OrderStatusDO orderStatus = getOrderStatusByCode("DRAFT");
			order.setOrderStatus(orderStatus);
			order.setCreatedBy(userVO.getUserName());
			order.setCreatedDate(new Date());
			order.setModifiedBy(userVO.getUserName());
			order.setModifiedDate(new Date());
			logger.info("Add  order>>>>>>>>>");
			getSession().saveOrUpdate(order);
			List<OrderItemVO> itemList = orderVO.getItemList();
			Set<OrderItemDO> items = new HashSet<OrderItemDO>();
			for (OrderItemVO itemVO : itemList) {
				OrderItemDO orderItem = mapper.map(itemVO,OrderItemDO.class);
				MenuDO menu =getMenuByCode(itemVO.getMenu().getMenuCode());
				logger.info("Add Item itemName>>>>>>>>>"+orderItem.getItemName());
				orderItem.setMenu(menu);
				orderItem.setCreatedBy(userVO.getUserName());
				orderItem.setCreatedDate(new Date());
				orderItem.setModifiedBy(userVO.getUserName());
				orderItem.setModifiedDate(new Date());
				orderItem.setOrder(order);
				orderItem.getOrder().setHotel(order.getHotel());
				orderItem.setOrderitemId(null);
				items.add(orderItem);
				getSession().saveOrUpdate(orderItem);
			}
			
			
		
		
		
		logger.info("Add Item successfully");
		}catch(Exception ex){
			logger.error("Error occured while placeOrder>>>>"+ex);
			throw new HotelDAOException("Error occured while placeOrder>>>>>>>>>>>"+ex);
		}
		return order.getOrderId();
	}
	@Override
	public HotelDO getHotelByCode(String hotelCode) throws HotelDAOException {
		logger.info("Entered getHotelByCode");
		HotelDO hotel =null;
		try{
		Query query = getSession().createQuery("from HotelDO hotel where hotel.hotelCode= '"+hotelCode+"'");
		 hotel =(HotelDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getHotelByCode>>>>"+ex);
			throw new HotelDAOException("Error occured while getHotelByCode>>>>>>>>>>>"+ex);
		}
		return hotel;
	}
	
	@Override
	public HotelDO getHotelByName(String hotelName) throws HotelDAOException {
		logger.info("Entered getHotelByName");
		HotelDO hotel =null;
		try{
		Query query = getSession().createQuery("from HotelDO hotel where hotel.hotelName= '"+hotelName+"'");
		 hotel =(HotelDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getHotelByName>>>>"+ex);
			throw new HotelDAOException("Error occured while getHotelByName>>>>>>>>>>>"+ex);
		}
		return hotel;
	}
	@Override
	public MenuDO getMenuByCode(String menuCode) throws HotelDAOException {
		logger.info("Entered getMenuByCode");
		MenuDO  menu=null;
		try{
		Query query =getSession().createQuery("from MenuDO menu where menu.menuCode= '"+menuCode+"'");
		 menu =(MenuDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getMenuByCode>>>>"+ex);
			throw new HotelDAOException("Error occured while getMenuByCode>>>>>>>>>>>"+ex);
		}
		return menu;
	}
	
	@Override
	public MenuDO getMenuByName(String menuName) throws HotelDAOException {
		logger.info("Entered getMenuByName");
		MenuDO  menu=null;
		try{
		Query query =getSession().createQuery("from MenuDO menu where menu.menuName= '"+menuName+"'");
		 menu =(MenuDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getMenuByName>>>>"+ex);
			throw new HotelDAOException("Error occured while getMenuByName>>>>>>>>>>>"+ex);
		}
		return menu;
	}
	@Override
	public ItemDO getItemByCode(String itemCode) throws HotelDAOException {
		logger.info("Entered getItemByCode");
		ItemDO item=null;
		try{
		Query query =getSession().createQuery("from ItemDO item where item.itemCode= '"+itemCode+"'");
		 item =(ItemDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getItemByCode>>>>"+ex);
			throw new HotelDAOException("Error occured while getItemByCode>>>>>>>>>>>"+ex);
		}
		return item;
	}
	@Override
	public List<ItemDO> searchMenu(String hotelName, String menuName,Date dateFrom,Date dateTo) throws HotelDAOException {
		logger.info("Entered searchMenu");
		List<ItemDO> items=null;
		List<ItemDO> itemList=new ArrayList<ItemDO>();
		List<MenuDO> menus =null;
		MenuDO  menu=null;
		Query menuquery = null;
		String dateFromStr = null;
		String dateToStr =null;
		try{
			if(dateFrom!=null && null!=dateTo){
				SimpleDateFormat dateFormat =new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				 dateFromStr = dateFormat.format(dateFrom);
				 dateToStr = dateFormat.format(dateTo);
			}
			if((null!=hotelName && !hotelName.isEmpty()) && (null==menuName||(null!=menuName && menuName.isEmpty()))){
				
				Query query =getSession().createQuery("from MenuDO menu where menu.hotel.hotelName= '"+hotelName+"'");
				menus =(List<MenuDO>)query.list();
				for (MenuDO menuDO : menus) {
					if(dateFrom!=null && null!=dateTo){
						 menuquery =getSession().createQuery("from MenuDO menu where menu.menuId= "+menuDO.getMenuId()+ " and menu.createdDate BETWEEN '"+dateFromStr+"' AND '"+dateToStr+"'");
					}else{
						
						 menuquery =getSession().createQuery("from MenuDO menu where menu.menuId= "+menuDO.getMenuId());
					}
					menu =(MenuDO)menuquery.list().get(0);
					if(null!=menu){
						
						Query searchquery =getSession().createQuery("from ItemDO item where item.menu.menuId="+menu.getMenuId());
						items =(List<ItemDO>)searchquery.list();
					}
					itemList.addAll(items);
				}
				return itemList;
			}
			if(null!=menuName && !menuName.isEmpty()){
				
				if(dateFrom!=null && null!=dateTo){
					 menuquery =getSession().createQuery("from MenuDO menu where menu.menuName= '"+menuName+"' and menu.createdDate BETWEEN '"+dateFromStr+"' AND '"+dateToStr+"'");
				}else{
					
					 menuquery =getSession().createQuery("from MenuDO menu where menu.menuName= '"+menuName+"'");
				}
				List<MenuDO> menuList = menuquery.list();
				if(menuList.size()>0){
					menu =(MenuDO)menuList.get(0);
					if(null!=menu){
						
						Query searchquery =getSession().createQuery("from ItemDO item where item.menu.menuId="+menu.getMenuId());
						items =(List<ItemDO>)searchquery.list();
					}
				}
				return items;
			}
			
		}catch(Exception ex){
			logger.error("Error occured while searchMenu>>>>"+ex);
			throw new HotelDAOException("Error occured while searchMenu>>>>>>>>>>>"+ex);
		}
		return items;
	}
	@Override
	public List<OrderDO> searchOrder(Date dateFrom, Date dateTo) throws HotelDAOException {
		logger.info("Entered searchOrder");
		List<OrderDO> orders=null;
		String dateFromStr = null;
		String dateToStr =null;
		try{
			if(dateFrom!=null && null!=dateTo){
				SimpleDateFormat dateFormat =new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				 dateFromStr = dateFormat.format(dateFrom);
				 dateToStr = dateFormat.format(dateTo);
			}
				
				Query query =getSession().createQuery("from OrderDO order where order.createdDate BETWEEN '"+dateFromStr+"' AND '"+dateToStr+"'");
				orders =(List<OrderDO>)query.list();
				return orders;
			
		}catch(Exception ex){
			logger.error("Error occured while searchOrder>>>>"+ex);
			throw new HotelDAOException("Error occured while searchOrder>>>>>>>>>>>"+ex);
		}
	}
	@Override
	public Long editOrder(OrderVO orderVO, UserVO userVO) throws HotelDAOException {
		logger.info("Entered editOrder---DAO");
		OrderDO order =null;
		HotelDO hotel =null;
		try{
		
				hotel = getHotelByCode(orderVO.getHotel().getHotelCode());
			
				order = mapper.map(orderVO,OrderDO.class);
				if(order.getOrderId()==null){
					order =getOrderByCode(orderVO.getOrderCode());
				}
				logger.info("Add Order order code>>>>>>>>>"+order.getOrderCode());
				order.setCreatedBy(userVO.getUserName());
				order.setCreatedDate(new Date());
				order.setModifiedBy(userVO.getUserName());
				order.setModifiedDate(new Date());
				order.setHotel(hotel);
				getSession().saveOrUpdate(order);
			
		
		
		
		logger.info("Edit order successfully");
		
		}catch(Exception ex){
			logger.error("Error occured while Edit order>>>>"+ex);
			throw new HotelDAOException("Error occured while Edit order>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit Edit Item");
		return order.getOrderId();
	}
	@Override
	public OrderDO getOrderByCode(String orderCode) throws HotelDAOException {
		logger.info("Entered getOrderByCode");
		OrderDO order =null;
		try{
		Query query = getSession().createQuery("from OrderDO order where order.orderCode= '"+orderCode+"'");
		 order =(OrderDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getOrderByCode>>>>"+ex);
			throw new HotelDAOException("Error occured while getOrderByCode>>>>>>>>>>>"+ex);
		}
		return order;
	}
	
	@Override
	public OrderDO getOrderByOrderId(Long orderId) throws HotelDAOException {
		logger.info("Entered getOrderByOrderId");
		OrderDO order =null;
		try{
		Query query = getSession().createQuery("from OrderDO order where order.orderId= "+orderId);
		 order =(OrderDO)query.uniqueResult();
		}catch(Exception ex){
			logger.error("Error occured while getOrderByOrderId>>>>"+ex);
			throw new HotelDAOException("Error occured while getOrderByOrderId>>>>>>>>>>>"+ex);
		}
		return order;
	}
	
	@Override
	public List<OrderItemDO> getOrderItemsByOrderId(Long orderId) throws HotelDAOException {
		logger.info("Entered getOrderItemsByOrderId");
		List<OrderItemDO> orderItems =null;
		try{
		Query query = getSession().createQuery("from OrderItemDO orderItem where orderItem.order.orderId= "+orderId);
		 orderItems =(List<OrderItemDO>)query.list();
		}catch(Exception ex){
			logger.error("Error occured while getOrderItemsByOrderId>>>>"+ex);
			throw new HotelDAOException("Error occured while getOrderItemsByOrderId>>>>>>>>>>>"+ex);
		}
		return orderItems;
	}
	@Override
	public OrderStatusDO getOrderStatusByCode(String orderStatusCode) throws HotelDAOException {
		logger.info("Entered getOrderStatusByCode");
		OrderStatusDO orderStatus =null;
		try{
		Query query = getSession().createQuery("from OrderStatusDO orderStatus where orderStatus.orderStatusCode= '"+orderStatusCode+"'");
		 orderStatus =(OrderStatusDO)query.list().get(0);
		}catch(Exception ex){
			logger.error("Error occured while getOrderStatusByCode>>>>"+ex);
			throw new HotelDAOException("Error occured while getOrderStatusByCode>>>>>>>>>>>"+ex);
		}
		return orderStatus;
	}
	public IRuleDAO getRuleDAO() {
		return ruleDAO;
	}
	public void setRuleDAO(IRuleDAO ruleDAO) {
		this.ruleDAO = ruleDAO;
	}
	@Override
	public List<ItemDO> searchMenuForCustomer(String hotelName) throws HotelDAOException {
		logger.info("Entered searchMenuForCustomer");
		List<ItemDO> items=null;
		List<ItemDO> itemList=new ArrayList<ItemDO>();
		List<MenuDO> menus =null;
		MenuDO  menu=null;
		Query menuquery = null;
		if(null!=hotelName && !hotelName.isEmpty()){
			HotelDO hotelDO = getHotelByName(hotelName);
							
							List<RuleDO> ruleList = getRuleDAO().getRuleByHotelId(hotelDO.getHotelId());
							LocalDate date = LocalDate.now();
							DayOfWeek dayOfWeek = date.getDayOfWeek();

							String dayName = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH);
							if(ruleList.size()>0){
								for (RuleDO ruleDO : ruleList) {
									if(ruleDO.getRuleStatus().equalsIgnoreCase("ACTIVE") && ruleDO.getRuleCondition().equalsIgnoreCase(dayName)){
										
										MenuDO searchMenu= getMenuByName(ruleDO.getRuleAction());
										if(null!=searchMenu){
											
											Query searchquery =getSession().createQuery("from ItemDO item where item.menu.menuId="+searchMenu.getMenuId());
											items =(List<ItemDO>)searchquery.list();
										}
										itemList.addAll(items);
									}
								}
			}else{
				Query query =getSession().createQuery("from MenuDO menu where menu.hotel.hotelName= '"+hotelName+"'");
				menus =(List<MenuDO>)query.list();
				for (MenuDO menuDO : menus) {
					menu =(MenuDO)menuquery.list().get(0);
					if(null!=menu){
						
						Query searchquery =getSession().createQuery("from ItemDO item where item.menu.menuId="+menu.getMenuId());
						items =(List<ItemDO>)searchquery.list();
					}
					itemList.addAll(items);
				}
			}
		}
		return itemList;
	}

}
