package com.stat9.ohms.hotel.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.AbstractDAO;

@Component
public class RuleDAO extends AbstractDAO implements IRuleDAO{

	@Autowired
	private Mapper mapper;
	
	
	private Log logger = LogFactory.getLog(RuleDAO.class);
	@Override
	public Long addRule(RuleDO ruleDO, HotelDO hotelDO) throws HotelDAOException {
		logger.info("Entered addRule");
		try{
			RuleDO ruleDB =getRuleByName(ruleDO.getRuleName());
			
			if(null==ruleDB){
				
				ruleDO.setHotel(hotelDO);
			
				getSession().save(ruleDO);
			}else{
				ruleDB.setHotel(hotelDO);
				ruleDB.setRuleStatus(ruleDO.getRuleStatus());
				ruleDB.setModifiedDate(new Date());
				ruleDB.setModifiedBy(ruleDO.getModifiedBy());
				getSession().update(ruleDB);
			}
			
			
		}catch(Exception ex){
			logger.error("Error occured while Add Rule>>>>"+ex);
			throw new HotelDAOException("Error occured while Add Rule>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit Add Menu");
		
		return ruleDO.getRuleId();
	}
	public Mapper getMapper() {
		return mapper;
	}
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	@Override
	public RuleDO getRuleByName(String ruleName) throws HotelDAOException {
		logger.info("Entered getRuleByName");
		RuleDO rule=null;
		try{
			Query query = getSession().createQuery("from RuleDO rule where rule.ruleName= '"+ruleName+"'");
			 rule =query.list().size()>0?(RuleDO)query.list().get(0):null;
			
		
		}catch(Exception ex){
			logger.error("Error occured while getRuleByName>>>>"+ex);
			throw new HotelDAOException("Error occured while getRuleByName>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit getRuleByName");
		
		return rule;
	}
	
	@Override
	public List<RuleDO> getRuleByHotelId(Long hotelId) throws HotelDAOException {
		logger.info("Entered getRuleByHotelId");
		List<RuleDO> ruleList=null;
		try{
			Query query = getSession().createQuery("from RuleDO rule where rule.hotel.hotelId= "+hotelId);
			 ruleList =query.list();
			
		
		}catch(Exception ex){
			logger.error("Error occured while getRuleByHotelId>>>>"+ex);
			throw new HotelDAOException("Error occured while getRuleByHotelId>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit getRuleByHotelId");
		
		return ruleList;
	}
	public List<MenuDO> getMenuByHotelName(String hotelName) throws HotelDAOException  {
		logger.info("Entered getMenuByHotelName");
		List<MenuDO>  menuList=null;
		try{
			Query query = getSession().createQuery("from MenuDO menu where menu.hotel.hotelName= '"+hotelName+"'");
			 menuList =(List<MenuDO>)query.list();
			
		
		}catch(Exception ex){
			logger.error("Error occured while getMenuByHotelName>>>>"+ex);
			throw new HotelDAOException("Error occured while getMenuByHotelName>>>>>>>>>>>>"+ex);
		}
		logger.info("Exit getMenuByHotelName");
		
		return menuList;
	}
	@Override
	public void deleteRule(RuleDO ruleDO) throws HotelDAOException {
		logger.info("Entered deleteRule");
		try{
				getSession().delete(ruleDO);
			
			
		}catch(Exception ex){
			logger.error("Error occured while deleteRule Rule>>>>"+ex);
			throw new HotelDAOException("Error occured while deleteRule>>>>>>>>>>>"+ex);
		}
		logger.info("Exit deleteRule");
		
		
	}

	
	
}
