package com.stat9.ohms.hotel.dao;

import java.util.List;

public interface IRuleDAO {

	Long addRule(RuleDO ruleDO, HotelDO hotelDO) throws HotelDAOException;

	RuleDO getRuleByName(String ruleName) throws HotelDAOException;
	
	List<RuleDO> getRuleByHotelId(Long hotelId) throws HotelDAOException;

	List<MenuDO> getMenuByHotelName(String hotelName) throws HotelDAOException;

	void deleteRule(RuleDO ruleDO) throws HotelDAOException;
}
