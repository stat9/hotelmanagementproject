package com.stat9.ohms.hotel.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.stat9.ohms.customer.dao.CustomerDO;


@Table(name = "ORDER_T")
@Entity
public class OrderDO {

	@Id
	@GeneratedValue
	@Column(name = "ORDER_ID")
	private Long orderId;

	@Column(name = "ORDER_CD")
	private String orderCode;
	
	@Column(name = "ORDER_TOTAL")
	private Double orderTotal;
	
	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private CustomerDO customer;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "HOTEL_ID",  referencedColumnName = "HOTEL_ID")
	private HotelDO hotel;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "ORDER_STATUS_ID", referencedColumnName = "ORDER_STATUS_ID")
	private OrderStatusDO orderStatus;


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
	private Set<OrderItemDO> itemList = new HashSet<OrderItemDO>();

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public CustomerDO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDO customer) {
		this.customer = customer;
	}

	public HotelDO getHotel() {
		return hotel;
	}

	public void setHotel(HotelDO hotel) {
		this.hotel = hotel;
	}



	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Set<OrderItemDO> getItemList() {
		return itemList;
	}

	public void setItemList(Set<OrderItemDO> itemList) {
		this.itemList = itemList;
	}

	public OrderStatusDO getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatusDO orderStatus) {
		this.orderStatus = orderStatus;
	}
}
