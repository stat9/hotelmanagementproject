package com.stat9.ohms.hotel.web.vo;

import com.stat9.ohms.customer.web.vo.AddressVO;
import com.stat9.ohms.customer.web.vo.PersonVO;

public class HotelVO {
	
	private Long hotelId;
	
	private String hotelName;
	private String hotelCode;
	private PersonVO person;

	private AddressVO address;

	public PersonVO getPerson() {
		return person;
	}

	public void setPerson(PersonVO person) {
		this.person = person;
	}

	public AddressVO getAddress() {
		return address;
	}

	public void setAddress(AddressVO address) {
		this.address = address;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}
}
