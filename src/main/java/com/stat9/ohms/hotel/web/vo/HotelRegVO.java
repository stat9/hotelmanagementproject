package com.stat9.ohms.hotel.web.vo;

import com.stat9.ohms.customer.web.vo.UserVO;

public class HotelRegVO {

	private UserVO user;
	private HotelVO hotel;
	
	public UserVO getUser() {
		return user;
	}
	public void setUser(UserVO user) {
		this.user = user;
	}
	public HotelVO getHotel() {
		return hotel;
	}
	public void setHotel(HotelVO hotel) {
		this.hotel = hotel;
	}
	
}
