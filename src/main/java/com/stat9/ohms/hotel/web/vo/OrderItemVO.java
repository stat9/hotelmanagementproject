package com.stat9.ohms.hotel.web.vo;

import java.util.Date;

public class OrderItemVO {

private Long itemId;
	
private Long orderitemId;

private String itemCode;

private String itemName;

private Double itemPrice;

private Integer itemQuantity;

private String createdBy;

private String modifiedBy;

private Date createdDate;

private Date modifiedDate;

private MenuVO menu;

private OrderVO order;

public Long getItemId() {
	return itemId;
}

public void setItemId(Long itemId) {
	this.itemId = itemId;
}

public Long getOrderitemId() {
	return orderitemId;
}

public void setOrderitemId(Long orderitemId) {
	this.orderitemId = orderitemId;
}

public String getItemCode() {
	return itemCode;
}

public void setItemCode(String itemCode) {
	this.itemCode = itemCode;
}

public String getItemName() {
	return itemName;
}

public void setItemName(String itemName) {
	this.itemName = itemName;
}

public Double getItemPrice() {
	return itemPrice;
}

public void setItemPrice(Double itemPrice) {
	this.itemPrice = itemPrice;
}

public Integer getItemQuantity() {
	return itemQuantity;
}

public void setItemQuantity(Integer itemQuantity) {
	this.itemQuantity = itemQuantity;
}

public String getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}

public String getModifiedBy() {
	return modifiedBy;
}

public void setModifiedBy(String modifiedBy) {
	this.modifiedBy = modifiedBy;
}

public Date getCreatedDate() {
	return createdDate;
}

public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
}

public Date getModifiedDate() {
	return modifiedDate;
}

public void setModifiedDate(Date modifiedDate) {
	this.modifiedDate = modifiedDate;
}

public MenuVO getMenu() {
	return menu;
}

public void setMenu(MenuVO menu) {
	this.menu = menu;
}

public OrderVO getOrder() {
	return order;
}

public void setOrder(OrderVO order) {
	this.order = order;
}

}
