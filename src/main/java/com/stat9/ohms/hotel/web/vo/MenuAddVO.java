package com.stat9.ohms.hotel.web.vo;

import com.stat9.ohms.customer.web.vo.UserVO;

public class MenuAddVO {

	private UserVO user;
	private MenuVO menu;
	
	public UserVO getUser() {
		return user;
	}
	public void setUser(UserVO user) {
		this.user = user;
	}
	public MenuVO getMenu() {
		return menu;
	}
	public void setMenu(MenuVO menu) {
		this.menu = menu;
	}
}
