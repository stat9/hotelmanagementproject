package com.stat9.ohms.hotel.web.vo;

public class ItemVO {

private Long itemId;
	
	private String itemName;
	
	private Double itemPrice;
	
	private Integer itemQuantity;
	
	private String itemCode;
	
	private MenuVO menu;
	
	private Double itemLineTotal;
	
	private String itemStatus;

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(Double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public Integer getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(Integer itemQuantity) {
		this.itemQuantity = itemQuantity;
	}


	public MenuVO getMenu() {
		return menu;
	}

	public void setMenu(MenuVO menu) {
		this.menu = menu;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Double getItemLineTotal() {
		return itemLineTotal;
	}

	public void setItemLineTotal(Double itemLineTotal) {
		this.itemLineTotal = itemLineTotal;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}
}
