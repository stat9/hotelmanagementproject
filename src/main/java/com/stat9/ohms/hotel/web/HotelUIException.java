package com.stat9.ohms.hotel.web;

public class HotelUIException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public HotelUIException() {
		super();
	}
	
	public HotelUIException(String message) {
		super(message);
	}
}
