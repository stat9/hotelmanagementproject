package com.stat9.ohms.hotel.web.vo;

import java.util.HashSet;
import java.util.Set;

public class MenuVO {

	private HotelVO hotel;
	
	private String menuName;
	
	private String menuCode;
	
	private Set<ItemVO> itemList = new HashSet<ItemVO>();

	public HotelVO getHotel() {
		return hotel;
	}

	public void setHotel(HotelVO hotel) {
		this.hotel = hotel;
	}

	public Set<ItemVO> getItemList() {
		return itemList;
	}

	public void setItemList(Set<ItemVO> itemList) {
		this.itemList = itemList;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

}
