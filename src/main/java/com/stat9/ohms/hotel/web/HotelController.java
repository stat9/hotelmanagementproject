package com.stat9.ohms.hotel.web;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.service.HotelService;
import com.stat9.ohms.hotel.service.HotelServiceException;
import com.stat9.ohms.hotel.web.vo.HotelRegVO;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuAddVO;
import com.stat9.ohms.hotel.web.vo.OrderAddVO;
import com.stat9.ohms.hotel.web.vo.OrderItemVO;
import com.stat9.ohms.hotel.web.vo.OrderStatusVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;


@Controller
public class HotelController {

	@Autowired
	private HotelService hotelService;

	private Log logger = LogFactory.getLog(HotelController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public String registerHotel(@RequestBody HotelRegVO hotel) throws HotelUIException {
		logger.info("Entered registerhotel--controller");
		Long hotelId = null;
		try {
			hotelId = getHotelService().registerHotel(hotel.getHotel(), hotel.getUser());
		} catch (HotelServiceException e) {
			logger.error("Error occured while registring hotel");
			throw new HotelUIException();

		}
		return hotelId != null ? "addMenu" : "error";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/deleteItemOrder")
	public void deleteItemFromOrder(@RequestBody OrderItemVO orderItemVO,@RequestBody OrderVO orderVO,@RequestBody UserVO userVO) throws HotelUIException {
		logger.info("Entered deleteItemFromOrder--controller");
		try {
			 getHotelService().deleteItemFromOrder(orderItemVO, orderVO, userVO);
		} catch (HotelServiceException e) {
			logger.error("Error occured while adeleteItemFromOrder");
			throw new HotelUIException();

		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/menu")
	public Long placeOrder(@RequestBody OrderAddVO order) throws HotelUIException {
		logger.info("Entered adding menu--controller");
		Long orderId = null;
		try {
			orderId = getHotelService().placeOrder(order.getOrder(), order.getUser());
		} catch (HotelServiceException e) {
			logger.error("Error occured while adding menu");
			throw new HotelUIException();

		}
		return orderId;

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/menu")
	public List<ItemVO> searchMenu(@RequestParam String hotelName, @RequestParam String menuName,@RequestParam Date dateFrom,@RequestParam Date dateTo) throws HotelUIException {
		logger.info("Entered search menu--controller");
		List<ItemVO> items = null;
		try {
			items = getHotelService().searchMenu(hotelName, menuName,dateFrom,dateTo);
		} catch (HotelServiceException e) {
			logger.error("Error occured while searching menu");
			throw new HotelUIException();

		}
		return items ;

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/menuForCustomer")
	public List<ItemVO> searchMenuForCustomer(@RequestParam String hotelName) throws HotelUIException {
		logger.info("Entered searchMenuForCustomer--controller");
		List<ItemVO> items = null;
		try {
			items = getHotelService().searchMenuForCustomer(hotelName);
		} catch (HotelServiceException e) {
			logger.error("Error occured while searchMenuForCustomer");
			throw new HotelUIException();

		}
		return items ;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/menu")
	public String addMenu(@RequestBody MenuAddVO menu) throws HotelUIException {
		logger.info("Entered adding menu--controller");
		Long orderId = null;
		try {
			orderId = getHotelService().addMenu(menu.getMenu(), menu.getUser());
		} catch (HotelServiceException e) {
			logger.error("Error occured while adding menu");
			throw new HotelUIException();

		}
		return orderId != null ? "menu" : "error";

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/item")
	public String addItem(@RequestBody MenuAddVO menu,@RequestBody ItemVO item) throws HotelUIException {
		logger.info("Entered adding item--controller");
		Long itemId = null;
		try {
			itemId = getHotelService().addItem(menu.getMenu(), menu.getUser(),item);
		} catch (HotelServiceException e) {
			logger.error("Error occured while adding itemr");
			throw new HotelUIException();

		}
		return itemId != null ? "addItem" : "error";

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/item")
	public String editItem(@RequestBody ItemVO item,@RequestBody UserVO user) throws HotelUIException {
		logger.info("Entered editing item--controller");
		Long itemId = null;
		try {
			itemId = getHotelService().editItem(item, user);
		} catch (HotelServiceException e) {
			logger.error("Error occured while editing itemr");
			throw new HotelUIException();

		}
		return itemId != null ? "searchmenu" : "error";

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/item")
	public String deleteItem(@RequestBody ItemVO item,@RequestBody UserVO user) throws HotelUIException {
		logger.info("Entered editing item--controller");
		Long itemId = null;
		try {
			itemId = getHotelService().deleteItem(item, user);
		} catch (HotelServiceException e) {
			logger.error("Error occured while editing itemr");
			throw new HotelUIException();

		}
		return itemId != null ? "searchmenu" : "error";

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/item")
	public String editOrder(@RequestBody OrderVO order,@RequestBody UserVO user) throws HotelUIException {
		logger.info("Entered editing order--controller");
		Long itemId = null;
		try {
			itemId = getHotelService().editOrder(order, user);
		} catch (HotelServiceException e) {
			logger.error("Error occured while editing order");
			throw new HotelUIException();

		}
		return itemId != null ? "searchorder" : "error";

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/order")
	public List<OrderVO> searchOrder(@RequestParam Date dateFrom,@RequestParam Date dateTo) throws HotelUIException {
		logger.info("Entered search order--controller");
		List<OrderVO> orders = null;
		try {
			orders = getHotelService().searchOrder(dateFrom,dateTo);
		} catch (HotelServiceException e) {
			logger.error("Error occured while searching order");
			throw new HotelUIException();

		}
		return orders ;

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getOrderStatus")
	public OrderStatusVO getOrderStatusByCode(String orderStatusCode)  throws HotelUIException {
		logger.info("Entered getOrderStatusByCode");
		OrderStatusVO orderStatusVO =null;
		
		try{
			orderStatusVO =getHotelService().getOrderStatusByCode(orderStatusCode);
		}catch(HotelServiceException ex){
			logger.error("Error occured while getOrderStatusByCode>>>>"+ex);
			throw new HotelUIException("Error occured while getOrderStatusByCode>>>>>>>>>>>"+ex);
		}
		return orderStatusVO;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getOrder")
	public OrderVO getOrderByOrderId(@RequestParam Long orderId) throws HotelUIException {
		logger.info("Entered get order--controller");
		OrderVO orderVO = null;
		try {
			orderVO = getHotelService().getOrderByOrderId(orderId);
			List<OrderItemVO> itemList = getHotelService().getOrderItemsByOrderId(orderId);
			orderVO.setItemList(itemList);
			
		} catch (HotelServiceException e) {
			logger.error("Error occured while getting order");
			throw new HotelUIException();

		}
		return orderVO ;

	}

	
	
	@RequestMapping(method = RequestMethod.POST, value = "/getItem")
	public ItemVO getItemByCode(@RequestParam String itemCode) throws HotelUIException {
		logger.info("Entered getItemByCode--controller");
		ItemVO item = null;
		try {
			item = getHotelService().getItemByCode(itemCode);
		} catch (HotelServiceException e) {
			logger.error("Error occured while getItemByCode");
			throw new HotelUIException();

		}
		return item ;

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getHotel")
	public HotelVO getHotelByCode(@RequestParam String hotelCode) throws HotelUIException {
		logger.info("Entered getHotelByCode--controller");
		HotelVO hotel = null;
		try {
			hotel = getHotelService().getHotelByCode(hotelCode);
		} catch (HotelServiceException e) {
			logger.error("Error occured while getHotelByCode");
			throw new HotelUIException();

		}
		return hotel ;

	}

	public HotelService getHotelService() {
		return hotelService;
	}

	public void setHotelService(HotelService hotelService) {
		this.hotelService = hotelService;
	}
}
