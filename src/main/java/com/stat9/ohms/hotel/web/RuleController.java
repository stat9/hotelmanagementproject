package com.stat9.ohms.hotel.web;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.service.HotelServiceException;
import com.stat9.ohms.hotel.service.RuleService;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.RuleVO;

@Controller
public class RuleController {
	@Autowired
	private RuleService ruleService;

	private Log logger = LogFactory.getLog(RuleController.class);
	
	@RequestMapping(method = RequestMethod.POST, value = "/addRule")
	public String addRule(@RequestBody RuleVO rule,@RequestBody HotelVO hotel,@RequestBody UserVO user) throws HotelUIException {
		logger.info("Entered addRule--controller");
		Long ruleId = null;
		try {
			ruleId = getRuleService().addRule(rule, hotel,user);
		} catch (HotelServiceException e) {
			logger.error("Error occured while addRule");
			throw new HotelUIException();

		}
		return ruleId != null ? "addRule" : "error";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getRule")
	public RuleVO getRuleByName(@RequestBody String ruleName) throws HotelUIException {
		logger.info("Entered getRuleByName--controller");
		RuleVO rule = null;
		try {
			rule = getRuleService().getRuleByName(ruleName);
		} catch (HotelServiceException e) {
			logger.error("Error occured while getRuleByName");
			throw new HotelUIException();

		}
		return rule;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/deleteRule")
	public void deleteRule(@RequestBody RuleVO rule) throws HotelUIException {
		logger.info("Entered deleteRule--controller");
		try {
			getRuleService().deleteRule(rule);
			
		} catch (HotelServiceException e) {
			logger.error("Error occured while deleteRule");
			throw new HotelUIException();

		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getMenuList")
	public List<MenuVO> getMenuByHotelName(@RequestBody String hotelName) throws HotelUIException {
		logger.info("Entered getMenuByHotelName--controller");
		List<MenuVO> menuList = null;
		try {
			menuList = getRuleService().getMenuByHotelName(hotelName);
		} catch (HotelServiceException e) {
			logger.error("Error occured while getMenuByHotelName");
			throw new HotelUIException();

		}
		return menuList;
	}

	public RuleService getRuleService() {
		return ruleService;
	}

	public void setRuleService(RuleService ruleService) {
		this.ruleService = ruleService;
	}
}
