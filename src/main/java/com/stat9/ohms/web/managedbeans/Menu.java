package com.stat9.ohms.web.managedbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.RandomNumberGenerator;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuAddVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;

@Component
@Scope("request")
public class Menu {
	@Autowired
	private HotelController hotelController;
	private Log logger = LogFactory.getLog(Menu.class);

	private String menuName;

	private  String menuCode;
	private boolean openAddItem =false;
	
	private List<ItemVO> itemList =null;
	private Set<ItemVO> items = new HashSet<ItemVO>();
	
	private Map<String,String> status;
	
	@PostConstruct
	public void init(){
		itemList = new ArrayList<ItemVO>();
		for(int i=0;i<=100;i++){
			ItemVO itemVO= new ItemVO();
			itemVO.setItemName("");
			itemVO.setItemPrice(0.0);
			itemList.add(itemVO);
		}
		
		status  = new HashMap<String, String>();
		status.put("AVAILABLE", "AVAILABLE");
		status.put("UNAVAILABLE", "UNAVAILABLE");
	}
	public String addMenu() throws HotelUIException {
		logger.info("Entered additem");
		FacesContext context = FacesContext.getCurrentInstance();
		setMenuCode("MN"+RandomNumberGenerator.getRandomNumber());
		String retString=null;
		try {
	
			MenuAddVO menu = new MenuAddVO();
			MenuVO menuVO = new MenuVO();
			menuVO.setMenuName(menuName);
			menu.setMenu(menuVO);
			menuVO.setMenuCode(getMenuCode());
			UserVO user= (UserVO)context.getExternalContext().getSessionMap().get("user");
			HotelVO hotel= (HotelVO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("hotel");
			for (ItemVO itemVO : itemList) {
				if((itemVO.getItemName()!=null && !itemVO.getItemName().isEmpty())&&(itemVO.getItemPrice()!=null && itemVO.getItemPrice()!=0.0)){
					itemVO.setItemCode("IT"+RandomNumberGenerator.getRandomNumber());
					items.add(itemVO);
				}
			}
			menuVO.setHotel(hotel);
			menuVO.setItemList(items);
			menu.setUser(user);
			retString= getHotelController().addMenu(menu);
			if(retString!=null){
				// Set the message here
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Menu added successfully", "addMenu"); 
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("menu", menuVO);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			return retString;
		} catch (HotelUIException ex) {
			logger.error("Error occured at additem" + ex);
			throw new HotelUIException("Error occured at additem " + ex);
		}

	}
	
	
	
	

	public HotelController getHotelController() {
		return hotelController;
	}

	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}


	public String getMenuName() {
		return menuName;
	}


	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public boolean isOpenAddItem() {
		return openAddItem;
	}
	public void setOpenAddItem(boolean openAddItem) {
		this.openAddItem = openAddItem;
	}
	public List<ItemVO> getItemList() {
		return itemList;
	}
	public void setItemList(List<ItemVO> itemList) {
		this.itemList = itemList;
	}
	public Set<ItemVO> getItems() {
		return items;
	}
	public void setItems(Set<ItemVO> items) {
		this.items = items;
	}
	public Map<String, String> getStatus() {
		return status;
	}
	public void setStatus(Map<String, String> status) {
		this.status = status;
	}
	
}
