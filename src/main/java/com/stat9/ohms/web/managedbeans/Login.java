package com.stat9.ohms.web.managedbeans;

import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.customer.web.vo.CustomerController;
import com.stat9.ohms.customer.web.vo.CustomerUIException;
import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.LoginController;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.LoginVO;

@Component
@Scope("session")
public class Login {
	private String userName;
	private String password;
	private Boolean loggedIn=false;
	private Boolean hotelLoggedIn=false;
	
	@Autowired
	private LoginController loginController;
	
	@Autowired
	private HotelController hotelController;
	
	@Autowired
	private CustomerController customerController;
	
	private Log logger = LogFactory.getLog(Login.class);
	public String validateUser() throws CustomerUIException{
		logger.info("Entered validateUser");
		String retString =null;
		try{
			UserVO user = new UserVO();
			user.setUserName(userName);
			user.setPassword(password);
			LoginVO  login = loginController.validateUser(user);
			if(null!=login){
				loggedIn=true;
				if(login.getReturnString()!=null && login.getReturnString().equalsIgnoreCase("hotel")){
					hotelLoggedIn = true;
					HotelVO hotel = hotelController.getHotelByCode(login.getUserVO().getUserCode());
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotel);
				}else{
					hotelLoggedIn =false;
					CustomerVO customer = customerController.getCustomerByCode(login.getUserVO().getUserCode());
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("customer", customer);
				}
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", login.getUserVO());
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("isLoggedIn", loggedIn);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("isHotelLoggedIn", hotelLoggedIn);
				retString =login.getReturnString();
			}else{
				loggedIn=false;
			}
		}catch(CustomerUIException | HotelUIException ex){
			logger.error("error while validate User");
			throw new CustomerUIException();
		}
		return retString!=null?retString:"error";
		
	}
	
	public void logout(){
		logger.info("User Logged out");
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", null);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("isLoggedIn", false);
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("isHotelLoggedIn", false);
	}
	
	public String customerRegistration() {
		return "customerRegistration";
	}
	
	public String hotelRegistration() {
		return "hotelRegistration";
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public LoginController getLoginController() {
		return loginController;
	}
	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	
	public HotelController getHotelController() {
		return hotelController;
	}

	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}

	public Boolean getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public Boolean getHotelLoggedIn() {
		return hotelLoggedIn;
	}

	public void setHotelLoggedIn(Boolean hotelLoggedIn) {
		this.hotelLoggedIn = hotelLoggedIn;
	}

	public CustomerController getCustomerController() {
		return customerController;
	}

	public void setCustomerController(CustomerController customerController) {
		this.customerController = customerController;
	}

	
}
