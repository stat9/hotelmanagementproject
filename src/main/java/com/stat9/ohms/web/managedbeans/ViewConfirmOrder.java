package com.stat9.ohms.web.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.customer.web.vo.CustomerController;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.OrderItemVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;

@Component
@Scope("request")
public class ViewConfirmOrder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private HotelController hotelController;

	@Autowired
	private CustomerController customerController;
	private Log logger = LogFactory.getLog(ViewConfirmOrder.class);
	
	private Long orderId;
	
	private String orderStatus;
	
	private Double orderTotal;
	
	private List<OrderItemVO> itemList;
	
	@PostConstruct
	public void init() throws HotelUIException{
		if(orderId==null)
		{
			orderId =(Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderId");
		}
	
		if(orderId!=null){
		getOrder();
			
		}
	
	}
	
	public String getOrder() throws HotelUIException{
		logger.info("Entered getOrder");
		OrderVO order=null;
		try {
			order = getHotelController().getOrderByOrderId(orderId);
			itemList =order.getItemList();
			orderTotal= order.getOrderTotal();
			if(null!=order.getOrderStatus()){
				
				orderStatus =order.getOrderStatus().getOrderStatusDesc();
			}
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", order);
			if (itemList != null && itemList.size() > 0) {
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("itemList", itemList);
			}

			logger.info("Items retrieved for Order>>>>>>>>>>>>>>>>>" + itemList.size());
		} catch (Exception ex) {
			logger.error("Error occured at getOrder" + ex);
			throw new HotelUIException("Error occured at getOrder " + ex);
		}

		return  "vieworder" ;
		
	}
	
	
	public HotelController getHotelController() {
		return hotelController;
	}
	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}
	public CustomerController getCustomerController() {
		return customerController;
	}
	public void setCustomerController(CustomerController customerController) {
		this.customerController = customerController;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public List<OrderItemVO> getItemList() {
		return itemList;
	}
	public void setItemList(List<OrderItemVO> itemList) {
		this.itemList = itemList;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

}
