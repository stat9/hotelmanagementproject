package com.stat9.ohms.web.managedbeans;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.RandomNumberGenerator;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuAddVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;

@Component
@Scope("request")
public class SearchMenu implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private HotelController hotelController;
	private Log logger = LogFactory.getLog(SearchMenu.class);

	private String menuName;

	private  String hotelName;
	
	private Date dateFrom;
	
	private Date dateTo;
	
	private List<ItemVO> itemList;
	
	private ItemVO selectedItem;
	
	private ItemVO itemToAdd = new ItemVO();
	
	private String itemName="";
	private Double itemPrice=0.00;
	
	private String addItemName="";
	private Double addItemPrice=0.00;
	private String addItemStatus="";
	private Long itemId;
	
	private MenuVO menu;
	private Map<String,String> status;
	private String itemStatus="";
	
	@PostConstruct
	public void init(){
		itemList = (List<ItemVO>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("itemList");		
		if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItem"))
			selectedItem=(ItemVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItem");
		if(null!=selectedItem && null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItemName"))
		selectedItem.setItemName((String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItemName"));
		if(null!=selectedItem && null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItemPrice"))
		selectedItem.setItemPrice((Double)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItemPrice"));
		if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemNameToAdd")){
			itemToAdd.setItemName((String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemNameToAdd"));
		}
			if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemPriceToAdd")){
				itemToAdd.setItemPrice((Double)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemPriceToAdd"));
			}
			
			if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemStatusToAdd")){
				itemToAdd.setItemStatus((String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemStatusToAdd"));
			}
			status  = new HashMap<String, String>();
			status.put("AVAILABLE", "AVAILABLE");
			status.put("UNAVAILABLE", "UNAVAILABLE");
	}
	
	public void onStatusChange(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItemStatus", getItemStatus());
	}
	
	public void onAddStatusChange(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("itemStatusToAdd", getAddItemStatus());
	}

	public String searchMenu() throws HotelUIException {
		logger.info("Entered searchMenu");
		try {
			itemList = getHotelController().searchMenu(hotelName, menuName,dateFrom,dateTo);
			if (itemList != null && itemList.size() > 0) {
				ItemVO item = itemList.get(0);
				 menu = item.getMenu();
				HotelVO hotel = menu.getHotel();
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotel);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("menu", menu);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("itemList", itemList);
			}

			logger.info("Items retrieved in search>>>>>>>>>>>>>>>>>" + itemList.size());
		} catch (Exception ex) {
			logger.error("Error occured at searchMenu" + ex);
			throw new HotelUIException("Error occured at searchMenu " + ex);
		}

		return itemList != null && itemList.size() > 0 ? "searchmenu" : "searchmenu";

	}

	public String editItem() throws HotelUIException{
		logger.info("Entered editItem");
		String retString="";
		try {
	
			UserVO user =(UserVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			if( null!=FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItemStatus")){
				itemStatus =(String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItemStatus");
			}
			selectedItem.setItemStatus(itemStatus);
			retString = getHotelController().editItem(selectedItem, user);
			if(null!=retString){
				
			logger.info("Item selected in search>>>>>>>>>>>>>>>>>" + selectedItem);
				MenuVO menu = selectedItem.getMenu();
				HotelVO hotel = menu.getHotel();
				FacesMessage msg = new FacesMessage("Your Item edited successfully");
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotel);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("menu", menu);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("item", selectedItem);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

		} catch (Exception ex) {
			logger.error("Error occured at editItem" + ex);
			throw new HotelUIException("Error occured at editItem " + ex);
		}

		return retString;
	}
	

	
	public void itemNameChanged(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItemName", getItemName());
	}
	
	public void itemPriceChanged(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItemPrice", getItemPrice());
	}
	
	public void itemNameToAdd(){
		logger.info("Item getAddItemName >>>>>>>>>>>>>>>>>" + getAddItemName());
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("itemNameToAdd", getAddItemName());
	}
	
	public void itemPriceToAdd(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("itemPriceToAdd", getAddItemPrice());
	}
	
	public void itemStatusToAdd(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("itemStatusToAdd", getAddItemStatus());
	}
	
	public String deleteItem() throws HotelUIException{
		logger.info("Entered deleteItem");
		String retString="";
		try {
			UserVO user =(UserVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			retString = getHotelController().deleteItem(selectedItem, user);
			if(null!=retString){
				
			logger.info("Item selected in search>>>>>>>>>>>>>>>>>" + selectedItem);
				MenuVO menu = selectedItem.getMenu();
				HotelVO hotel = menu.getHotel();
				FacesMessage msg = new FacesMessage("Your Item edited successfully");
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotel);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("menu", menu);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("item", selectedItem);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

		} catch (Exception ex) {
			logger.error("Error occured at deleteItem" + ex);
			throw new HotelUIException("Error occured at deleteItem " + ex);
		}

		return retString;
	}
	
	public String addItem() throws HotelUIException{
		logger.info("Entered addItem");
		String retString="";
		
		try {
			if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemNameToAdd")){
				itemToAdd.setItemName((String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemNameToAdd"));
			}
				if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemPriceToAdd")){
					itemToAdd.setItemPrice((Double)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemPriceToAdd"));
				}
				
				if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemStatusToAdd")){
					itemToAdd.setItemStatus((String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemStatusToAdd"));
				}
				if(null!=FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("menu")){
					menu =(MenuVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("menu");
				}
				itemToAdd.setItemCode("IT" + RandomNumberGenerator.getRandomNumber());
			UserVO user =(UserVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			MenuAddVO menuAddVO = new MenuAddVO();
			menu.getItemList().add(itemToAdd);
			menuAddVO.setUser(user);
			menuAddVO.setMenu(menu);
			retString = getHotelController().addItem(menuAddVO,itemToAdd);
			if(null!=retString){
				FacesMessage msg = new FacesMessage("Your Item added successfully");
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", menu.getHotel());
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("menu", menu);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

		} catch (Exception ex) {
			logger.error("Error occured at addItem" + ex);
			throw new HotelUIException("Error occured at addItem " + ex);
		}

		return retString;
	}

	
	public HotelController getHotelController() {
		return hotelController;
	}

	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}


	public String getMenuName() {
		return menuName;
	}


	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public List<ItemVO> getItemList() {
		return itemList;
	}
	public void setItemList(List<ItemVO> itemList) {
		this.itemList = itemList;
	}
	public ItemVO getSelectedItem() {
		return selectedItem;
	}
	public void setSelectedItem(ItemVO selectedItem) {
		this.selectedItem = selectedItem;
	}
	
	public void updateItem(SelectEvent event) {
       selectedItem = ((ItemVO) event.getObject());
    }
	
	public void updateEditItem() {
		setItemId(selectedItem.getItemId());
		setItemPrice(selectedItem.getItemPrice());
		setItemName(selectedItem.getItemName());
		setItemStatus(selectedItem.getItemStatus());
		
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItemId", getItemId());
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItem",selectedItem);
	
	    }
 
    public void onRowUnselect(UnselectEvent event) {
    	 selectedItem =null;
    }



	public MenuVO getMenu() {
		return menu;
	}



	public void setMenu(MenuVO menu) {
		this.menu = menu;
	}






	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	



	public Long getItemId() {
		return itemId;
	}



	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}



	public Double getItemPrice() {
		return itemPrice;
	}



	public void setItemPrice(Double itemPrice) {
		this.itemPrice = itemPrice;
	}



	public ItemVO getItemToAdd() {
		return itemToAdd;
	}



	public void setItemToAdd(ItemVO itemToAdd) {
		this.itemToAdd = itemToAdd;
	}



	public String getAddItemName() {
		return addItemName;
	}



	public void setAddItemName(String addItemName) {
		this.addItemName = addItemName;
	}



	public Double getAddItemPrice() {
		return addItemPrice;
	}



	public void setAddItemPrice(Double addItemPrice) {
		this.addItemPrice = addItemPrice;
	}



	public Map<String, String> getStatus() {
		return status;
	}



	public void setStatus(Map<String, String> status) {
		this.status = status;
	}



	public String getItemStatus() {
		return itemStatus;
	}



	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getAddItemStatus() {
		return addItemStatus;
	}

	public void setAddItemStatus(String addItemStatus) {
		this.addItemStatus = addItemStatus;
	}




	
}
