package com.stat9.ohms.web.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.RuleController;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.RuleVO;

@Component
@Scope("request")
public class Rule  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private RuleController ruleController;
	private Log logger = LogFactory.getLog(Rule.class);
	
	private String ruleName ="";
	private String searchRuleName ="";
	private String ruleCondition="";
	
	private String ruleStatus ="";
	
	private String ruleAction;
	
	private List<MenuVO> menu;
	
	private Map<String,String> menuMap;
	
	private List<RuleVO> ruleList;
	
	private RuleVO selectedRule;
	private Map<String,String> status;
	private HotelVO hotel =null;
	
	@PostConstruct
	public void init(){
		 hotel = (HotelVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
					.get("hotel");
		if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedRule"))
			selectedRule=(RuleVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedRule");
		
		status  = new HashMap<String, String>();
		status.put("ACTIVE", "ACTIVE");
		status.put("INACTIVE", "INACTIVE");
	}
	
	
	public String searchRule()throws HotelUIException{
		String result="";
		try{
			selectedRule =getRuleController().getRuleByName(searchRuleName);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedRule",selectedRule);
		}catch(HotelUIException e){
			logger.error("Error occured while addRule");
			throw new HotelUIException();
		}
		return result;
	}
	
	
	
	public String editRule() throws HotelUIException{
		RuleVO rule =new RuleVO();
		String result="";
		try{
			UserVO user =(UserVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
					.get("user");
			rule.setRuleName(selectedRule.getRuleName());
			rule.setRuleStatus(selectedRule.getRuleStatus());
			result= getRuleController().addRule(rule, hotel,user);
			if(result!=null){
				// Set the message here
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Rule updated successfully", "rule"); 
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("rule", rule);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
		}catch(HotelUIException e){
			logger.error("Error occured while addRule");
			throw new HotelUIException();
		}
		
		return result;
	}
	

	
	public void deleteRule() throws HotelUIException{
		try{
			getRuleController().deleteRule(selectedRule);
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Rule deleted successfully", "rule"); 
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}catch(HotelUIException e){
			logger.error("Error occured while delete rule");
			throw new HotelUIException();
		}
		
	}
	
	/*public void updateEditRule() {
		setRuleName(selectedRule.getRuleName());
		setRuleStatus(selectedRule.getRuleStatus());
		
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedRule",selectedRule);
	
	    }

	public void onRowSelect(SelectEvent event) {
		selectedRule = ((RuleVO) event.getObject());
	    }*/
	public RuleController getRuleController() {
		return ruleController;
	}
	public void setRuleController(RuleController ruleController) {
		this.ruleController = ruleController;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getRuleCondition() {
		return ruleCondition;
	}
	public void setRuleCondition(String ruleCondition) {
		this.ruleCondition = ruleCondition;
	}
	public String getRuleAction() {
		return ruleAction;
	}
	public void setRuleAction(String ruleAction) {
		this.ruleAction = ruleAction;
	}
	public List<MenuVO> getMenu() {
		return menu;
	}
	public void setMenu(List<MenuVO> menu) {
		this.menu = menu;
	}

	public String getRuleStatus() {
		return ruleStatus;
	}

	public void setRuleStatus(String ruleStatus) {
		this.ruleStatus = ruleStatus;
	}

	public RuleVO getSelectedRule() {
		return selectedRule;
	}

	public void setSelectedRule(RuleVO selectedRule) {
		this.selectedRule = selectedRule;
	}

	public List<RuleVO> getRuleList() {
		return ruleList;
	}

	public void setRuleList(List<RuleVO> ruleList) {
		this.ruleList = ruleList;
	}

	public Map<String, String> getMenuMap() {
		return menuMap;
	}

	public void setMenuMap(Map<String, String> menuMap) {
		this.menuMap = menuMap;
	}


	public Map<String, String> getStatus() {
		return status;
	}


	public void setStatus(Map<String, String> status) {
		this.status = status;
	}


	public String getSearchRuleName() {
		return searchRuleName;
	}


	public void setSearchRuleName(String searchRuleName) {
		this.searchRuleName = searchRuleName;
	}

}
