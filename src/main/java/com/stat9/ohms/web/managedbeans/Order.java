package com.stat9.ohms.web.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.RandomNumberGenerator;
import com.stat9.ohms.customer.web.vo.CustomerController;
import com.stat9.ohms.customer.web.vo.CustomerUIException;
import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.OrderAddVO;
import com.stat9.ohms.hotel.web.vo.OrderItemVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;


@Component
@Scope("request")
public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private HotelController hotelController;

	@Autowired
	private CustomerController customerController;
	private Log logger = LogFactory.getLog(Order.class);

	private String hotelName;

	private String menuName;

	private List<ItemVO> itemList;
	
	private Long orderId;
	private List<ItemVO> selectedItems;
    
	private Double orderTotal=0.0;

	public void onRowSelect(SelectEvent event) {
		selectedItems.add((ItemVO) event.getObject());
		logger.info("items selected>>>>>>>>>>" + selectedItems.size());
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItems", selectedItems);
	}

	public void onRowUnselect(UnselectEvent event) {
		selectedItems.remove(((ItemVO) event.getObject()).getItemId());
		logger.info("items selected>>>>>>>>>>" + selectedItems.size());
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItems", selectedItems);
	}

	@PostConstruct
	public void init() {
		itemList = (List<ItemVO>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("itemList");
	}
    
	public String searchMenu() throws HotelUIException {
		logger.info("Entered searchMenu");
		try {
			itemList = getHotelController().searchMenuForCustomer(hotelName);
			if (itemList != null && itemList.size() > 0) {
				ItemVO item = itemList.get(0);
				MenuVO menu = item.getMenu();
				HotelVO hotel = menu.getHotel();
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("hotel", hotel);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("itemList", itemList);
			}

			logger.info("Items retrieved in search>>>>>>>>>>>>>>>>>" + itemList.size());
		} catch (Exception ex) {
			logger.error("Error occured at searchMenu" + ex);
			throw new HotelUIException("Error occured at searchMenu " + ex);
		}

		return itemList != null && itemList.size() > 0 ? "order" : "error";

	}

	
	public void placeOrder() throws HotelUIException {
		logger.info("Entered addOrder");
		List<OrderItemVO> orderitemList = new ArrayList<OrderItemVO>();
		try {
			OrderAddVO orderAddVO = new OrderAddVO();
			OrderVO orderVO = new OrderVO();
			orderVO.setOrderCode("OR" + RandomNumberGenerator.getRandomNumber());

			logger.info("items selected>>>>>>>>>>" + selectedItems.size());
			for (ItemVO itemVO : selectedItems) {
				OrderItemVO orderItem = new OrderItemVO();
				orderItem.setItemName(itemVO.getItemName());
				orderItem.setItemQuantity(itemVO.getItemQuantity());
				orderItem.setItemPrice(itemVO.getItemPrice());
				orderItem.setItemId(itemVO.getItemId());
				orderItem.setMenu(itemVO.getMenu());
				orderitemList.add(orderItem);
			}
			orderVO.setItemList(orderitemList);
			orderAddVO.setOrder(orderVO);
			UserVO user = (UserVO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			HotelVO hotel = (HotelVO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
					.get("hotel");
			CustomerVO customerVO = getCustomerController().getCustomerByCode(user.getUserCode());
			orderVO.setCustomer(customerVO);
			orderAddVO.setUser(user);
			orderVO.setHotel(hotel);
			orderTotal =(Double) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderTotal");
			orderVO.setOrderTotal(orderTotal);
			 orderId=  getHotelController().placeOrder(orderAddVO);
			
			if (orderId != null) {
				// Set the message here
				FacesMessage msg = new FacesMessage("Your Order #"+orderId+" placed successfully. Please Click on View Order for confirming the order.Please use this order number for further reference");
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", orderVO);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderId", orderId);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				 
			}
		} catch (HotelUIException ex) {
			logger.error("Error occured at addOrder" + ex);
			throw new HotelUIException("Error occured at addOrder " + ex);
		} catch (CustomerUIException e) {
			logger.error("Error occured at addOrder" + e);
			throw new HotelUIException("Error occured at addOrder " + e);
		}


	}
	
	
	
	public void caluclateOrderTotal(){
		//selectedItems =(List<ItemVO>)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItems");
		if(null!=selectedItems){
			
			for (ItemVO itemVO : selectedItems) {
				
				orderTotal = orderTotal+(itemVO.getItemPrice()*itemVO.getItemQuantity());
			}
		}
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderTotal", orderTotal);
	}

	

	public HotelController getHotelController() {
		return hotelController;
	}

	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public CustomerController getCustomerController() {
		return customerController;
	}

	public void setCustomerController(CustomerController customerController) {
		this.customerController = customerController;
	}

	
	public List<ItemVO> getItemList() {
		return itemList;
	}

	public void setItemList(List<ItemVO> itemList) {
		this.itemList = itemList;
	}

	public List<ItemVO> getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(List<ItemVO> selectedItems) {
		this.selectedItems = selectedItems;
	}

	public Double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}


}
