package com.stat9.ohms.web.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.customer.web.vo.CustomerController;
import com.stat9.ohms.customer.web.vo.CustomerUIException;
import com.stat9.ohms.customer.web.vo.CustomerVO;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.OrderAddVO;
import com.stat9.ohms.hotel.web.vo.OrderItemVO;
import com.stat9.ohms.hotel.web.vo.OrderVO;

@Component
@Scope("request")
public class ViewPlacedOrder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private HotelController hotelController;

	@Autowired
	private CustomerController customerController;
	private Log logger = LogFactory.getLog(ViewPlacedOrder.class);
	
	private Long orderId;
	
	private String orderStatus;
	
	private OrderItemVO selectedItem;
    
	private Double orderTotal=0.0;
	
	private List<OrderItemVO> itemList;
	
	private UserVO userVO;
	
	@PostConstruct
	public void init() throws HotelUIException{
		
		userVO=(UserVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
		if(orderId==null)
		{
			orderId =(Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderId");
		}
	
		if(orderId!=null){
		getOrder();
			
		}
		if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItem"))
			selectedItem=(OrderItemVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItem");
		if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderitemList"))
			itemList=(List<OrderItemVO>)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderitemList");
		if(null!= FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderTotal"))
			orderTotal=(Double)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderTotal");
	
	}
	
	public void updateOrderItem(SelectEvent event) {
	       selectedItem = ((OrderItemVO) event.getObject());
	       FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("selectedItem",selectedItem);
	    }
	
	public String getOrder() throws HotelUIException{
		logger.info("Entered getOrder");
		OrderVO order=null;
		try {
			order = getHotelController().getOrderByOrderId(orderId);
			itemList =order.getItemList();
			orderTotal= order.getOrderTotal();
			if(null!=order.getOrderStatus()){
				
				orderStatus =order.getOrderStatus().getOrderStatusDesc();
			}
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", order);
			if (itemList != null && itemList.size() > 0) {
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderitemList", itemList);
			}

			logger.info("Items retrieved for Order>>>>>>>>>>>>>>>>>" + itemList.size());
		} catch (Exception ex) {
			logger.error("Error occured at getOrder" + ex);
			throw new HotelUIException("Error occured at getOrder " + ex);
		}

		return  "viewPlacedOrder" ;
		
	}
	

	public String deleteItem() throws HotelUIException{
		logger.info("Entered deleteItem");
		OrderVO orderVO=null;
		try {
			orderVO = getHotelController().getOrderByOrderId(orderId);
			if(null!=selectedItem){
				getHotelController().deleteItemFromOrder(selectedItem, orderVO, userVO);
				itemList.remove(selectedItem);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderitemList",itemList);
			}
			caluclateOrderTotal();
			FacesMessage msg = new FacesMessage("Your Item deleted from Order #"+orderId+" successfully.");
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", userVO);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", orderVO);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderId", orderId);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception ex) {
			logger.error("Error occured at deleteItem" + ex);
			throw new HotelUIException("Error occured at deleteItem " + ex);
		}

		return  "viewPlacedOrder" ;
		
	}
	
	public String updateItem() throws HotelUIException{
		logger.info("Entered updateItem");
		OrderVO orderVO=null;
		try {
			orderVO = getHotelController().getOrderByOrderId(orderId);
			for (OrderItemVO orderItemVO : orderVO.getItemList()) {
				if(null!=selectedItem && orderItemVO.getOrderitemId()==selectedItem.getOrderitemId()){
					orderItemVO.setItemQuantity(selectedItem.getItemQuantity());
				}
			}
			caluclateOrderTotal();
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderitemList",itemList);
			getHotelController().editOrder(orderVO, userVO);
			FacesMessage msg = new FacesMessage("Your Item Updated for Order #"+orderId+" successfully.");
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", userVO);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", orderVO);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderId", orderId);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception ex) {
			logger.error("Error occured at updateItem" + ex);
			throw new HotelUIException("Error occured at updateItem " + ex);
		}

		return  "viewPlacedOrder" ;
		
	}
	
	public void caluclateOrderTotal(){
		//selectedItems =(List<ItemVO>)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("selectedItems");
		if(null!=itemList){
			
			for (OrderItemVO itemVO : itemList) {
				
				orderTotal = orderTotal+(itemVO.getItemPrice()*itemVO.getItemQuantity());
			}
		}
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderTotal", orderTotal);
	}
	
	public void placeOrder() throws HotelUIException {
		logger.info("Entered placeOrder");
		OrderVO order = null;
		try {
			OrderAddVO orderAddVO = new OrderAddVO();
			order = getHotelController().getOrderByOrderId(orderId);
			 List<OrderItemVO> itemListDB =order.getItemList();
			logger.info("items retrieved>>>>>>>>>>" + itemListDB.size());
			order.setItemList(null);
			if(null==itemList){
				itemList=(List<OrderItemVO>)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderitemList"); 
			}
			order.setItemList(itemList);
			order.setOrderTotal(orderTotal);
			orderAddVO.setOrder(order);
			UserVO user = (UserVO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			HotelVO hotel = (HotelVO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
					.get("hotel");
			CustomerVO customerVO = getCustomerController().getCustomerByCode(user.getUserCode());
			order.setCustomer(customerVO);
			orderAddVO.setUser(user);
			order.setHotel(hotel);
			orderTotal =(Double) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("orderTotal");
			order.setOrderTotal(orderTotal);
			 orderId=  getHotelController().placeOrder(orderAddVO);
			
			if (orderId != null) {
				// Set the message here
				FacesMessage msg = new FacesMessage("Your Order #"+orderId+" placed successfully. Please use this order number for further reference");
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("order", order);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("orderId", orderId);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				 
			}
		} catch (HotelUIException ex) {
			logger.error("Error occured at placeOrder" + ex);
			throw new HotelUIException("Error occured at placeOrder " + ex);
		} catch (CustomerUIException e) {
			logger.error("Error occured at placeOrder" + e);
			throw new HotelUIException("Error occured at placeOrder " + e);
		}


	}
	
	public HotelController getHotelController() {
		return hotelController;
	}
	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}
	public CustomerController getCustomerController() {
		return customerController;
	}
	public void setCustomerController(CustomerController customerController) {
		this.customerController = customerController;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public List<OrderItemVO> getItemList() {
		return itemList;
	}
	public void setItemList(List<OrderItemVO> itemList) {
		this.itemList = itemList;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}


	public UserVO getUserVO() {
		return userVO;
	}

	public void setUserVO(UserVO userVO) {
		this.userVO = userVO;
	}

	public OrderItemVO getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(OrderItemVO selectedItem) {
		this.selectedItem = selectedItem;
	}

}
