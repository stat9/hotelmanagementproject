package com.stat9.ohms.web.managedbeans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.common.RandomNumberGenerator;
import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelController;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.ItemVO;
import com.stat9.ohms.hotel.web.vo.MenuAddVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;

@Component
@Scope("request")
public class Item {
	private Log logger = LogFactory.getLog(Item.class);
	@Autowired
	private HotelController hotelController;
	
	
	private List<ItemVO> itemList = new ArrayList<ItemVO>();
	private Set<ItemVO> items = new HashSet<ItemVO>();
	
	private String itemName;
	
	private Double itemPrice;
	
	private String itemCode;
	
	@PostConstruct
	public void init(){
		
	}
	public String addItem() throws HotelUIException {
		logger.info("Entered additem");
		setItemCode("IT"+RandomNumberGenerator.getRandomNumber());
		FacesContext context = FacesContext.getCurrentInstance();
		ItemVO item = new ItemVO();
		String retString =null;
		try {
			item.setItemCode(itemCode);
			item.setItemName(getItemName());
			item.setItemPrice(getItemPrice());
			itemList.add(item);
			items.add(item);
			MenuAddVO menu = new MenuAddVO();
			MenuVO menuVO = (MenuVO)context.getExternalContext().getSessionMap().get("menu");
			HotelVO hotel= (HotelVO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("hotel");
			menuVO.setItemList(items);
			menuVO.setHotel(hotel);
			item.setMenu(menuVO);
			menu.setMenu(menuVO);
			UserVO user= (UserVO)context.getExternalContext().getSessionMap().get("user");
			menu.setUser(user);
			retString= getHotelController().addItem(menu,item);
			if(retString!=null){
				// Set the message here
				FacesMessage msg = new FacesMessage("Your item added successfully"); 
				
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("item", item);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("menu", menuVO);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			return retString;
		} catch (HotelUIException ex) {
			logger.error("Error occured at additem" + ex);
			throw new HotelUIException("Error occured at additem " + ex);
		}

	}
	public String backToHomePage(){
		return "backToHomePage";
	}
	public HotelController getHotelController() {
		return hotelController;
	}
	public void setHotelController(HotelController hotelController) {
		this.hotelController = hotelController;
	}
	public List<ItemVO> getItemList() {
	
		return itemList;
	}
	public void setItemList(List<ItemVO> itemList) {
			this.itemList = itemList;
	}
	public Set<ItemVO> getItems() {
		return items;
	}
	public void setItems(Set<ItemVO> items) {
		this.items = items;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(Double itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
}
