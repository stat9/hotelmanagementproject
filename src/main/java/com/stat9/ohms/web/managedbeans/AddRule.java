package com.stat9.ohms.web.managedbeans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.stat9.ohms.customer.web.vo.UserVO;
import com.stat9.ohms.hotel.web.HotelUIException;
import com.stat9.ohms.hotel.web.RuleController;
import com.stat9.ohms.hotel.web.vo.HotelVO;
import com.stat9.ohms.hotel.web.vo.MenuVO;
import com.stat9.ohms.hotel.web.vo.RuleVO;

@Component
@Scope("request")
public class AddRule  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private RuleController ruleController;
	private Log logger = LogFactory.getLog(AddRule.class);
	
	private String ruleName;
	
	private String ruleCondition;
	
	private String ruleStatus;
	
	private String ruleAction;
	
	private List<MenuVO> menu;
	
	private Map<String,String> menuMap;
	private Map<String,String> status;
	
	private HotelVO hotel =null;
	@PostConstruct
	public void init() throws HotelUIException {
		 hotel = (HotelVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("hotel");
			status  = new HashMap<String, String>();
			status.put("ACTIVE", "ACTIVE");
			status.put("INACTIVE", "INACTIVE");
		try {
			menu = getRuleController().getMenuByHotelName(hotel.getHotelName());
			menuMap  = new HashMap<String, String>();
			for (MenuVO menuVO : menu) {
				menuMap.put(menuVO.getMenuName(), menuVO.getMenuName());
			}
		} catch (HotelUIException e) {
			logger.error("Error occured while getMenuByHotelName");
			throw new HotelUIException();
		}
	}

	public String addRule() throws HotelUIException{
		RuleVO rule =new RuleVO();
		String result="";
		try{
			rule.setRuleAction(ruleAction);
			rule.setRuleCondition(ruleCondition);
			rule.setRuleName(ruleName);
			rule.setRuleStatus(ruleStatus);
			UserVO user =(UserVO)FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
					.get("user");
			result= getRuleController().addRule(rule, hotel,user);
			if(result!=null){
				// Set the message here
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Rule added successfully", "addRule"); 
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("rule", rule);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
		}catch(HotelUIException e){
			logger.error("Error occured while addRule");
			throw new HotelUIException();
		}
		
		return result;
	}
	public RuleController getRuleController() {
		return ruleController;
	}

	public void setRuleController(RuleController ruleController) {
		this.ruleController = ruleController;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleCondition() {
		return ruleCondition;
	}

	public void setRuleCondition(String ruleCondition) {
		this.ruleCondition = ruleCondition;
	}

	public String getRuleStatus() {
		return ruleStatus;
	}

	public void setRuleStatus(String ruleStatus) {
		this.ruleStatus = ruleStatus;
	}

	public String getRuleAction() {
		return ruleAction;
	}

	public void setRuleAction(String ruleAction) {
		this.ruleAction = ruleAction;
	}

	public List<MenuVO> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuVO> menu) {
		this.menu = menu;
	}

	public Map<String, String> getMenuMap() {
		return menuMap;
	}

	public void setMenuMap(Map<String, String> menuMap) {
		this.menuMap = menuMap;
	}

	public HotelVO getHotel() {
		return hotel;
	}

	public void setHotel(HotelVO hotel) {
		this.hotel = hotel;
	}

	public Map<String, String> getStatus() {
		return status;
	}

	public void setStatus(Map<String, String> status) {
		this.status = status;
	}
}
