package com.stat9.ohms.login.service;

public interface ILoginService {
	String validateUser(String userName,String password) throws LoginServiceException;
}
