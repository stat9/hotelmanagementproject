package com.stat9.ohms.login.service;

public class LoginServiceException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public LoginServiceException() {
		super();
	}
	
	public LoginServiceException(String message) {
		super(message);
	}
}
