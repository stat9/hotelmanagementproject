package com.stat9.ohms.login.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.stat9.ohms.hotel.web.vo.LoginVO;
import com.stat9.ohms.login.dao.LoginDAO;
import com.stat9.ohms.login.dao.LoginDAOException;

@Component
public class LoginService {
	
	@Autowired
	private LoginDAO loginDAO;
	private Log logger = LogFactory.getLog(LoginService.class);
	
	@Transactional
	public LoginVO validateUser(String userName, String password) throws LoginServiceException {
		logger.info("Entered validateUser");
		LoginVO loginVO =null;
		try{
			loginVO = getLoginDAO().validateUser(userName, password);
		}catch(LoginDAOException ex){
			logger.error("error occured while validating user>>>>>>>>"+ex);
			throw new LoginServiceException("error occured while validating user>>>>>>>>"+ex);
		}
		logger.info("exit validateUser");
		return loginVO;
	}

	public LoginDAO getLoginDAO() {
		return loginDAO;
	}

	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}

}
