package com.stat9.ohms.login.dao;

public class LoginDAOException extends Exception {

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public LoginDAOException() {
		super();
	}
	
	public LoginDAOException(String message) {
		super(message);
	}
}
