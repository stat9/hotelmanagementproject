package com.stat9.ohms.login.dao;

import com.stat9.ohms.hotel.web.vo.LoginVO;

public interface ILoginDAO {

	LoginVO validateUser(String userName,String password) throws LoginDAOException;
}
